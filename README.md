# README

_kaiaulu_: the Hawaiian word for community.

This is a small social networking site. Users can sign up, send messages, post pictures, submit blog posts, search/filter and then read blog posts, update their user profile, browse other user profiles, view the latest weather in their city, etc.

## App Information

App Name: kaiaulu

Created: April-May 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/kaiaulu)

Production: [Link](https://kaiaulu.mctighecw.site)

## Tech Stack

_Frontend_

- React
- Redux
- React Router
- Less & CSS Modules
- [Prettier](https://prettier.io/)

_Backend_

- Flask
- JWT authentication
- SQLAlchemy
- PostgreSQL
- [OpenWeatherMap API](https://openweathermap.org/api) for weather info
- [Cloudinary](https://cloudinary.com/) for image management

## Credits

- Login & Sign Up page background image courtesy of [Tyler Lastovich](https://www.pexels.com/photo/beach-buildings-city-clouds-412681/)
- Mail icons courtesy of _Those Icons_ on [Flaticon](https://www.flaticon.com/authors/those-icons)
- Other icons courtesy of _Gregor Cresnar_ on [Flaticon](https://www.flaticon.com/authors/gregor-cresnar)

## To Run

### From terminal

_Backend_

1. Install virtualenv (with Python 3.6) and Python requirements

```
$ cd backend
$ virtualenv -p python3 venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

2. Initialize database

```
(venv) $ source scripts/create_db_dev.sh
```

3. Start backend

```
(venv) $ FLASK_ENV=development python $(pwd)/run.py
```

_Frontend_

1. Install requirements

```
$ cd frontend
$ yarn install
```

2. Start frontend

```
$ yarn start
```

### On Docker

_Production_

```
$ docker-compose up -d --build
$ source docker_init_db.sh
```

Project running at `localhost:80`

Last updated: 2025-02-08
