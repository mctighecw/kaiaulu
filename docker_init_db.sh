#!/bin/bash

echo Initializing Docker production db...

docker exec -it kaiaulu_postgresql_1 /bin/bash /usr/src/scripts/create_db.sh
docker exec -it kaiaulu_kaiaulu-backend_1 /bin/bash scripts/init_docker_db.sh
