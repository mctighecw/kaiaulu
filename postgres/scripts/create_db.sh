#!/usr/bin/env bash

echo Setting up $FLASK_ENV database...

echo Creating user: $DB_USER. Please enter password: $DB_PW_PROD
createuser -U $DB_ADMIN -P $DB_USER

echo Creating new db...
createdb -U $DB_ADMIN -E 'utf8' -T 'template0' $DB_NAME
