#!/bin/bash

cd backend
source venv/bin/activate
FLASK_ENV=development python $(pwd)/run.py
