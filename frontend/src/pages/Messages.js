import React from 'react';
import Topbar from 'Containers/Topbar';
import Messages from 'Components/Messages/Messages';

const MessagesPage = () => (
  <div>
    <Topbar />
    <Messages />
  </div>
);

export default MessagesPage;
