import React from 'react';
import Topbar from 'Containers/Topbar';
import About from 'Components/About/About';

const AboutPage = () => (
  <div>
    <Topbar />
    <About />
  </div>
);

export default AboutPage;
