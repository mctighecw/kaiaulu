import React from 'react';
import Topbar from 'Containers/Topbar';
import Dashboard from 'Containers/Dashboard/Dashboard';
import Footer from 'Components/Footer/Footer';

const DashboardPage = () => (
  <div>
    <Topbar />
    <Dashboard />
    <Footer />
  </div>
);

export default DashboardPage;
