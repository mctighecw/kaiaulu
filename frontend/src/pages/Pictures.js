import React from 'react';
import Topbar from 'Containers/Topbar';
import Pictures from 'Components/Pictures/Pictures';

const PicturesPage = () => (
  <div>
    <Topbar />
    <Pictures />
  </div>
);

export default PicturesPage;
