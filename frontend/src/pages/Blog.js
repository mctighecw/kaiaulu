import React from 'react';
import Topbar from 'Containers/Topbar';
import Blog from 'Containers/Blog';

const BlogPage = () => (
  <div>
    <Topbar />
    <Blog />
  </div>
);

export default BlogPage;
