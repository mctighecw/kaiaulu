import React from 'react';
import Topbar from 'Containers/Topbar';
import Component404 from 'Components/Component404/Component404';

const Page404 = () => (
  <div>
    <Topbar />
    <Component404 />
  </div>
);

export default Page404;
