import React from 'react';
import SignUp from 'Components/Auth/SignUp/SignUp';

class SignUpPage extends React.Component {
  render() {
    const { history, signUpUser } = this.props;

    return <SignUp history={history} signUpUser={signUpUser} />;
  }
}

export default SignUpPage;
