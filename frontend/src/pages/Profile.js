import React from 'react';
import Topbar from 'Containers/Topbar';
import Profile from 'Containers/Profile';

const ProfilePage = () => (
  <div>
    <Topbar />
    <Profile />
  </div>
);

export default ProfilePage;
