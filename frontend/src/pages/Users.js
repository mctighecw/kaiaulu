import React from 'react';
import Topbar from 'Containers/Topbar';
import Users from 'Components/Users/Users';

const UsersPage = () => (
  <div>
    <Topbar />
    <Users />
  </div>
);

export default UsersPage;
