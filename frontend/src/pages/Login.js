import React from 'react';
import Login from 'Components/Auth/Login/Login';

class LoginPage extends React.Component {
  render() {
    const { history, auth, loginUser } = this.props;

    return <Login history={history} loginUser={loginUser} authError={auth.authenticationError} />;
  }
}

export default LoginPage;
