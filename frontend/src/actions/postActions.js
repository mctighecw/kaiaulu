import * as types from './actionTypes';

export const updateAllPosts = (data) => ({
  type: types.UPDATE_ALL_POSTS,
  data,
});

export const updateUserPosts = (data) => ({
  type: types.UPDATE_USER_POSTS,
  data,
});

export const updatePostsFilter = (data) => ({
  type: types.UPDATE_FILTER,
  data,
});
