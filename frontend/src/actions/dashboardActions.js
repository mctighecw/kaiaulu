import * as types from './actionTypes';

export const updateCounts = (data) => ({
  type: types.UPDATE_COUNTS,
  data,
});

export const updateNewPicCount = (value) => ({
  type: types.UPDATE_NEW_PIC_COUNT,
  value,
});

export const updateRecentPosts = (data) => ({
  type: types.UPDATE_RECENT_POSTS,
  data,
});

export const updateWeather = (data) => ({
  type: types.UPDATE_WEATHER,
  data,
});
