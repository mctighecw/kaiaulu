import * as types from './actionTypes';

export const setUserInfo = (data) => ({
  type: types.SET_USER_INFO,
  data,
});

export const clearUserInfo = () => ({
  type: types.CLEAR_USER_INFO,
});
