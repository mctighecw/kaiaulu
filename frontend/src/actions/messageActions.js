import * as types from './actionTypes';

export const saveAllUsers = (data) => ({
  type: types.SAVE_ALL_USERS,
  data,
});

export const updateReceivedMessages = (data) => ({
  type: types.UPDATE_RECEIVED_MESSAGES,
  data,
});

export const updateSentMessages = (data) => ({
  type: types.UPDATE_SENT_MESSAGES,
  data,
});

export const updateDeletedMessages = (data) => ({
  type: types.UPDATE_DELETED_MESSAGES,
  data,
});

export const updateUnreadCount = (value) => ({
  type: types.UPDATE_UNREAD_COUNT,
  value,
});
