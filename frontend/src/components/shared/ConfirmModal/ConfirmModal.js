import React from 'react';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import CloseIcon from 'Assets/close-icon.svg';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

const ConfirmModal = ({ action, type, onClickCancel, onClickConfirm }) => (
  <div styleName="styles.modal-background">
    <div styleName="styles.box">
      <img src={CloseIcon} alt="" styleName="styles.close" onClick={onClickCancel} />

      <div styleName="styles.content">
        <div styleName="styles.text">{`Are you sure you want to ${action} this ${type}?`}</div>

        <SmallButton label={action} onClickMethod={onClickConfirm} />

        <div styleName="shared.button-spacing" />

        <SmallButton label="Cancel" onClickMethod={onClickCancel} />
      </div>
    </div>
  </div>
);

export default ConfirmModal;
