import React from 'react';
import './styles.less';

const TextBox = ({ rows, name, placeholder, value, maxLength, onChangeMethod }) => (
  <div styleName="container">
    <textarea
      rows={rows}
      name={name}
      placeholder={placeholder}
      value={value}
      maxLength={maxLength}
      onChange={onChangeMethod}
      styleName="text-box"
    />
  </div>
);

export default TextBox;
