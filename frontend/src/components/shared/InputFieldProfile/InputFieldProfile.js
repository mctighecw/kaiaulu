import React from 'react';
import './styles.less';

const InputFieldProfile = ({ type, value, name, maxLength, disabled, placeholder, onChangeMethod }) => (
  <div styleName="container">
    <input
      type={type}
      value={value}
      name={name}
      maxLength={maxLength}
      disabled={disabled || false}
      placeholder={placeholder}
      onChange={onChangeMethod}
    />
  </div>
);

export default InputFieldProfile;
