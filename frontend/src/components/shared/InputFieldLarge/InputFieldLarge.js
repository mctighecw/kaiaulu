import React from 'react';
import './styles.less';

const InputFieldLarge = ({ type, value, name, maxLength, placeholder, onChangeMethod }) => (
  <div styleName="container">
    <input
      type={type}
      value={value}
      name={name}
      maxLength={maxLength}
      placeholder={placeholder}
      onChange={onChangeMethod}
    />
  </div>
);

export default InputFieldLarge;
