import React from 'react';
import { getUrl } from 'Network/urls';
import { requestGet } from 'Network/requests';
import './styles.less';

import Main from 'Containers/Dashboard/Main';
import Sidebar from 'Containers/Dashboard/Sidebar';

class Dashboard extends React.Component {
  getCounts = () => {
    return new Promise((resolve, reject) => {
      const url = getUrl('get_counts');

      requestGet(url)
        .then((res) => {
          this.props.handleUpdateCounts(res.counts);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  getUnreadMessageCount = () => {
    return new Promise((resolve, reject) => {
      const url = getUrl('get_unread_number');

      requestGet(url)
        .then((res) => {
          this.props.handleUpdateUnreadCount(res.unread_count);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  getNewPicturesCount = () => {
    return new Promise((resolve, reject) => {
      const url = getUrl('new_picture_count');

      requestGet(url)
        .then((res) => {
          this.props.handleUpdateNewPicCount(res.new_pic_count);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  getRecentPosts = () => {
    return new Promise((resolve, reject) => {
      const url = getUrl('get_recent_posts');

      requestGet(url)
        .then((res) => {
          this.props.handleUpdateRecentPosts(res.posts);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  getWeatherInfo = () => {
    return new Promise((resolve, reject) => {
      const url = getUrl('get_weather_info');

      requestGet(url)
        .then((res) => {
          console.log(res);
          if (res.weather) {
            this.props.handleUpdateWeather(res.weather);
          }
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  makeAllRequests = async () => {
    await this.getCounts();
    await this.getUnreadMessageCount();
    await this.getNewPicturesCount();
    await this.getRecentPosts();
    await this.getWeatherInfo();
  };

  componentDidMount() {
    this.makeAllRequests();
  }

  render() {
    const { history, main, dashboard } = this.props;

    return (
      <div styleName="container">
        <div styleName="sidebar">
          <Sidebar dashboard={dashboard} />
        </div>
        <div styleName="main">
          <Main />
        </div>
      </div>
    );
  }
}

export default Dashboard;
