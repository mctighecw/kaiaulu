import React from 'react';
import { formatDateOnly, formatDateLong } from 'Utils/functions';
import Weather from 'Containers/Weather';
import shared from '../../../styles/shared.less';
import styles from './styles.less';

class Main extends React.Component {
  render() {
    const { history, main, dashboard, messages } = this.props;
    const dateToday = formatDateOnly(new Date());

    return (
      <div styleName="styles.container">
        <div styleName="styles.date">{dateToday}</div>

        <div styleName="styles.greeting">{`Welcome back, ${main.userInfo.first_name}!`}</div>

        <Weather />

        {messages.unreadCount > 0 && (
          <div styleName="styles.new-messages" onClick={() => history.push('/messages')}>
            {`Unread messages: ${messages.unreadCount}`}
          </div>
        )}

        {dashboard.newPics > 0 && (
          <div styleName="styles.new-messages" onClick={() => history.push('/pictures')}>
            {`New pictures: ${dashboard.newPics}`}
          </div>
        )}

        <div styleName="styles.extra-margin" />

        <div styleName="shared.medium-heading">Recent Blog Posts</div>

        <div styleName="styles.posts-container">
          {dashboard.recentPosts.length > 0 ? (
            dashboard.recentPosts.map((item, index) => (
              <div key={index} styleName="styles.post" onClick={() => history.push('/blog')}>
                <div styleName="styles.title">{item.title}</div>
                <div styleName="styles.user-date">{`${item.user.full_name} - ${formatDateLong(
                  item.created_at
                )}`}</div>
              </div>
            ))
          ) : (
            <div styleName="styles.no-posts">None yet</div>
          )}
        </div>
      </div>
    );
  }
}

export default Main;
