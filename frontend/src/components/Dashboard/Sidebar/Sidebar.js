import React from 'react';
import './styles.less';

const Sidebar = ({ dashboard }) => {
  const counts = [
    {
      label: 'Users',
      data: dashboard.counts.users,
    },
    {
      label: 'Blog posts',
      data: dashboard.counts.posts,
    },
    {
      label: 'Pictures',
      data: dashboard.counts.pictures,
    },
    {
      label: 'Messages sent',
      data: dashboard.counts.messages,
    },
  ];

  return (
    <div styleName="container">
      <div styleName="stats">
        <div styleName="header">SITE STATS</div>

        {counts.map((item, index) => (
          <div key={index} styleName="count">
            {`${item.label}: ${item.data}`}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
