import React from 'react';
import { Image, CloudinaryContext } from 'cloudinary-react';
import getUserInfo from 'Constants/getUserInfo';
import blankUserPic from 'Assets/blank-user.png';
import './styles.less';

const cloudName = process.env.CLOUD_NAME;

const DisplayProfile = ({ user }) => {
  const userInfo = getUserInfo(user);

  return (
    <div styleName="profile-box">
      <div styleName="picture">
        {user.pic_url && user.pic_url !== '' ? (
          <CloudinaryContext cloudName={cloudName}>
            <Image publicId={user.pic_url} crop="scale" styleName="img" />
          </CloudinaryContext>
        ) : (
          <img src={blankUserPic} alt="Blank" styleName="img" />
        )}
      </div>

      <div styleName="info">
        {userInfo.map((item, index) => (
          <div key={index} styleName="line">
            <div styleName="field">{`${item.label}:`}</div>
            <div styleName="value">{item.value}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DisplayProfile;
