import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';

import DropDown from 'Components/shared/DropDown/DropDown';
import DisplayProfile from './DisplayProfile/DisplayProfile';

import { sortUserOptions, limitOptions } from 'Constants/options';

import shared from '../../styles/shared.less';
import styles from './styles.less';

class Users extends React.Component {
  state = {
    numberUsers: 5,
    sortBy: 'newest',
    users: [],
  };

  getUsersInfo = () => {
    const { numberUsers, sortBy } = this.state;
    const parsedNumber = parseInt(numberUsers, 10);
    const url = getUrl('get_users');
    const data = JSON.stringify({
      sort_by: sortBy,
      max_number: parsedNumber,
    });

    requestPost(url, data)
      .then((res) => {
        this.setState({ users: res.users });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleSelectChange = (field, value) => {
    this.setState({ [field]: value }, () => {
      this.getUsersInfo();
    });
  };

  componentDidMount() {
    this.getUsersInfo();
  }

  render() {
    const { numberUsers, sortBy, users } = this.state;

    return (
      <div styleName="styles.container">
        <div styleName="shared.very-big-heading">Users List</div>

        <div styleName="styles.sort-limit">
          <div styleName="styles.dropdown">
            <div styleName="styles.label">Sort by</div>
            <DropDown
              value={sortBy}
              options={sortUserOptions}
              onChangeMethod={(event) => this.handleSelectChange('sortBy', event.target.value)}
            />
          </div>

          <div styleName="styles.dropdown">
            <div styleName="styles.label">Limit</div>
            <DropDown
              value={numberUsers}
              options={limitOptions}
              onChangeMethod={(event) => this.handleSelectChange('numberUsers', event.target.value)}
            />
          </div>
        </div>

        <div styleName="styles.users">
          {users && users.length > 0 ? (
            users.map((item, index) => <DisplayProfile key={index} user={item} />)
          ) : (
            <div>Loading...</div>
          )}
        </div>
      </div>
    );
  }
}

export default Users;
