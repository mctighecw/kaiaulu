import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';

import SmallButton from 'Components/shared/SmallButton/SmallButton';
import NewPost from './NewPost/NewPost';
import BlogPosts from 'Containers/BlogPosts';

import shared from '../../styles/shared.less';
import styles from './styles.less';

class Blog extends React.Component {
  state = {
    showForm: false,
  };

  getBlogPosts = () => {
    return new Promise((resolve, reject) => {
      const { numberPosts, sortBy, searchTerm } = this.props.posts.filter;
      const url = getUrl('get_blog_posts');
      const data = JSON.stringify({
        type: 'all',
        number_posts: numberPosts,
        sort_by: sortBy,
        search_term: searchTerm,
      });

      requestPost(url, data)
        .then((res) => {
          this.props.handleUpdateAllPosts(res.posts);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  render() {
    const { showForm } = this.state;

    return (
      <div styleName="styles.container">
        <div styleName="shared.very-big-heading">Blog</div>

        {showForm ? (
          <NewPost
            getBlogPosts={this.getBlogPosts}
            handleClickClose={() => this.setState({ showForm: false })}
          />
        ) : (
          <SmallButton label="New post" onClickMethod={() => this.setState({ showForm: true })} />
        )}

        <div styleName="styles.buffer" />
        <div styleName="shared.medium-heading">All Posts</div>

        <BlogPosts type="all" />
      </div>
    );
  }
}

export default Blog;
