import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import InputFieldLarge from 'Components/shared/InputFieldLarge/InputFieldLarge';
import TextBox from 'Components/shared/TextBox/TextBox';
import SmallButton from 'Components/shared/SmallButton/SmallButton';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

class NewPost extends React.Component {
  state = {
    title: '',
    body: '',
    error: '',
  };

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleSubmitPost = () => {
    const { title, body } = this.state;

    this.setState({ error: '' });

    if (title !== '' && body !== '') {
      const url = getUrl('create_post');
      const data = JSON.stringify({
        title,
        body,
      });

      requestPost(url, data)
        .then((res) => {
          this.setState({ title: '', body: '' });
          this.props.getBlogPosts();
          this.props.handleClickClose();
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      this.setState({ error: 'Please fill in title and body' });
    }
  };

  render() {
    const { title, body, error } = this.state;

    return (
      <div styleName="styles.container">
        <div styleName="shared.medium-heading">New Post</div>

        <InputFieldLarge
          type="text"
          value={title}
          name="title"
          placeholder="Title"
          maxLength="40"
          onChangeMethod={this.handleFieldChange}
        />

        <TextBox
          rows="10"
          value={body}
          name="body"
          placeholder="Body"
          maxLength="3000"
          onChangeMethod={this.handleFieldChange}
        />

        <div styleName="shared.error">{error}</div>

        <SmallButton label="Submit" onClickMethod={this.handleSubmitPost} />

        <div styleName="shared.button-spacing" />

        <SmallButton label="Cancel" onClickMethod={this.props.handleClickClose} />
      </div>
    );
  }
}

export default NewPost;
