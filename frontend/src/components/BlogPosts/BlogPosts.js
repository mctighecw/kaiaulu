import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import { formatDateLong } from 'Utils/functions';

import SmallButton from 'Components/shared/SmallButton/SmallButton';
import InputFieldSearch from 'Components/shared/InputFieldSearch/InputFieldSearch';
import DropDown from 'Components/shared/DropDown/DropDown';
import ConfirmModal from 'Components/shared/ConfirmModal/ConfirmModal';
import { sortBlogOptionsAll, sortBlogOptionsUser, limitOptions } from 'Constants/options';

import TrashCan from 'Assets/trash-can.svg';
import './styles.less';

class BlogPosts extends React.Component {
  constructor(props) {
    super(props);
    const { numberPosts, sortBy, searchTerm } = props.posts.filter;

    this.state = {
      numberPosts,
      sortBy,
      searchTerm,
      showModal: false,
      id: '',
    };
  }

  getBlogPosts = () => {
    return new Promise((resolve, reject) => {
      const { type } = this.props;
      const { numberPosts, sortBy, searchTerm } = this.state;
      const numberParsed = parseInt(numberPosts, 10);
      const url = getUrl('get_blog_posts');
      const data = JSON.stringify({
        type,
        number_posts: numberParsed,
        sort_by: sortBy,
        search_term: searchTerm,
      });

      requestPost(url, data)
        .then((res) => {
          console.log(res);
          if (type === 'all') this.props.handleUpdateAllPosts(res.posts);
          if (type === 'user') this.props.handleUpdateUserPosts(res.posts);
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  handleDeletePost = () => {
    const { id } = this.state;
    const url = getUrl('delete_post');
    const data = JSON.stringify({ id });

    requestPost(url, data)
      .then((res) => {
        this.setState({ showModal: false, id: '' });
        this.getBlogPosts();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleSelectChangeSortBy = (event) => {
    const data = { sortBy: event.target.value };
    this.setState(data, () => {
      this.props.handleUpdatePostsFilter(data);
      this.getBlogPosts();
    });
  };

  handleSelectChangeFilter = (event) => {
    const data = { numberPosts: event.target.value };
    this.setState(data, () => {
      this.props.handleUpdatePostsFilter(data);
      this.getBlogPosts();
    });
  };

  handleChangeSearchTerm = (event) => {
    this.setState({ searchTerm: event.target.value });
  };

  handleSendSearchTerm = () => {
    const data = { searchTerm: this.state.searchTerm };
    this.props.handleUpdatePostsFilter(data);
    this.getBlogPosts();
  };

  handleClearSearchTerm = () => {
    const data = { searchTerm: '' };
    this.setState(data, () => {
      this.props.handleUpdatePostsFilter(data);
      this.getBlogPosts();
    });
  };

  componentDidMount() {
    this.getBlogPosts();
  }

  render() {
    const { numberPosts, sortBy, searchTerm, showModal } = this.state;
    const { location, main, type, posts } = this.props;
    const sortByOptions = type === 'all' ? sortBlogOptionsAll : sortBlogOptionsUser;
    let data = [];

    if (type === 'all') {
      data = posts.allPosts;
    } else if (type === 'user') {
      data = posts.userPosts;
    }

    return (
      <div styleName="container">
        <div styleName="sort-limit">
          <div styleName="dropdown">
            <div styleName="label">Sort by</div>
            <DropDown value={sortBy} options={sortByOptions} onChangeMethod={this.handleSelectChangeSortBy} />
          </div>

          <div styleName="dropdown">
            <div styleName="label">Limit</div>
            <DropDown
              value={numberPosts}
              options={limitOptions}
              onChangeMethod={this.handleSelectChangeFilter}
            />
          </div>

          {type === 'all' && (
            <div styleName="search-bar">
              <div styleName="dropdown">
                <div styleName="label">Search</div>
                <InputFieldSearch
                  type="text"
                  value={searchTerm}
                  name="searchTerm"
                  placeholder="Enter term"
                  maxLength="30"
                  onChangeMethod={this.handleChangeSearchTerm}
                />
              </div>

              <SmallButton
                label="Search"
                onClickMethod={this.handleSendSearchTerm}
                styleName="search-button"
              />

              {searchTerm !== '' && (
                <div styleName="clear-search" onClick={this.handleClearSearchTerm}>
                  Clear search
                </div>
              )}
            </div>
          )}
        </div>

        <div styleName="posts">
          {data.length > 0 ? (
            data.map((item, index) => (
              <div key={index} styleName="post">
                {(main.userInfo.id === item.user.id || main.userInfo.is_admin) && (
                  <img
                    src={TrashCan}
                    alt="Delete"
                    title="Delete"
                    styleName="delete"
                    onClick={() => this.setState({ showModal: true, id: item.id })}
                  />
                )}
                <div styleName="title">{item.title}</div>
                <div styleName="user-date">
                  {`${item.user.full_name} - ${formatDateLong(item.created_at)}`}
                </div>
                <div styleName="body">{item.body}</div>
              </div>
            ))
          ) : (
            <div styleName="no-posts">No posts</div>
          )}

          {showModal && (
            <ConfirmModal
              action="delete"
              type="post"
              onClickCancel={() => this.setState({ showModal: false, id: '' })}
              onClickConfirm={this.handleDeletePost}
            />
          )}
        </div>
      </div>
    );
  }
}

export default BlogPosts;
