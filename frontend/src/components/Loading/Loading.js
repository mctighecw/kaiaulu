import React from 'react';
import LoadingSpinenr from 'Assets/loading-spinner.svg';
import './styles.less';

const Loading = () => (
  <div styleName="container">
    <img src={LoadingSpinenr} alt="spinner" />
    <p>Loading</p>
  </div>
);

export default Loading;
