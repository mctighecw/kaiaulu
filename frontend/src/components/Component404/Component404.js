import React from 'react';
import './styles.less';

const Component404 = () => (
  <div styleName="container">
    <div styleName="content">404 | How did you get here?</div>
  </div>
);

export default Component404;
