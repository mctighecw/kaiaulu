import React from 'react';
import getProfileInfo from 'Constants/getProfileInfo';
import './styles.less';

const DisplayProfile = ({ main }) => {
  const profileInfo = getProfileInfo(main);

  return (
    <div styleName="container">
      <div styleName="content">
        {profileInfo.map((item, index) => (
          <div key={index} styleName="line">
            <div styleName="field">{`${item.label}:`}</div>
            <div styleName="value">{item.value}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DisplayProfile;
