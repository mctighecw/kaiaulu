import React from 'react';
import { Image, CloudinaryContext } from 'cloudinary-react';

import SmallButton from 'Components/shared/SmallButton/SmallButton';
import DisplayProfile from './DisplayProfile/DisplayProfile';
import UploadPic from './UploadPic/UploadPic';
import EditProfile from './EditProfile/EditProfile';
import ChangePassword from 'Components/ChangePassword/ChangePassword';
import BlogPosts from 'Containers/BlogPosts';

import blankUserPic from 'Assets/blank-user.png';

import shared from '../../styles/shared.less';
import styles from './styles.less';

const cloudName = process.env.CLOUD_NAME;

class Profile extends React.Component {
  state = {
    editProfile: false,
  };

  render() {
    const { editProfile, userPic } = this.state;
    const { main, updateUserInfo } = this.props;

    return (
      <div styleName="styles.container">
        <div styleName="shared.very-big-heading">User Profile</div>

        <div styleName="styles.user-profile">
          {editProfile ? (
            <UploadPic />
          ) : (
            <div styleName="styles.picture">
              {main && main.userInfo && main.userInfo.pic_url && main.userInfo.pic_url !== '' ? (
                <CloudinaryContext cloudName={cloudName}>
                  <Image publicId={main.userInfo.pic_url} crop="scale" styleName="styles.img" />
                </CloudinaryContext>
              ) : (
                <img src={blankUserPic} alt="Blank" styleName="styles.img" />
              )}
            </div>
          )}

          {editProfile ? (
            <EditProfile
              main={main}
              updateUserInfo={updateUserInfo}
              onClickClose={() => this.setState({ editProfile: false })}
            />
          ) : (
            <div styleName="styles.display-profile">
              <DisplayProfile main={main} />

              <SmallButton label="Edit profile" onClickMethod={() => this.setState({ editProfile: true })} />
            </div>
          )}
        </div>

        <ChangePassword main={main} />

        <div styleName="styles.posts-heading">My Blog Posts</div>

        <BlogPosts type="user" />
      </div>
    );
  }
}

export default Profile;
