import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import { UPLOAD_SUCCESS, UPLOAD_ERROR, UPLOAD_NOT_ALLOWED } from 'Constants/messages';
import './styles.less';

const UploadPic = () => {
  const [status, setStatus] = useState([{ type: '', message: '' }]);

  const handleUploadPicture = (data) => {
    const url = getUrl('upload_picture');

    requestPost(url, data, false)
      .then((res) => {
        setStatus({ type: 'success', message: UPLOAD_SUCCESS });
      })
      .catch((err) => {
        console.log(err);
        setStatus({ type: 'error', message: UPLOAD_ERROR });
      });
  };

  const onDrop = (files) => {
    const multipleFiles = files.length === 0;

    if (multipleFiles) {
      setStatus({ type: 'error', message: UPLOAD_NOT_ALLOWED });
    } else {
      const formData = new FormData();
      formData.append('file', files[0]);
      formData.append('type', 'profile');
      handleUploadPicture(formData);
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/jpeg, image/png, image/gif',
    multiple: false,
    onDrop,
  });

  return (
    <div styleName="container">
      <div styleName="drop-zone">
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          {isDragActive ? <p>Drop your picture here</p> : <p>Click to select a picture or drag one here</p>}
        </div>
      </div>

      <div styleName="directions">Allowed file types are jpg/jpeg, png, gif</div>

      <div styleName={`status ${status.type ? status.type : ''}`}>{status.message}</div>
    </div>
  );
};

export default UploadPic;
