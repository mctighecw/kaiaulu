import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import InputFieldProfile from 'Components/shared/InputFieldProfile/InputFieldProfile';
import getProfileInfo from 'Constants/getProfileInfo';

import SmallButton from 'Components/shared/SmallButton/SmallButton';
import shared from '../../../styles/shared.less';
import styles from './styles.less';

class EditProfile extends React.Component {
  state = {
    first_name: '',
    last_name: '',
    email: '',
    city: '',
    state: '',
    country: '',
    birthday: '',
    favorite_color: '',
    hobbies: '',
    created_at: '',
  };

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleUpdateProfile = () => {
    const { first_name, last_name, city, state, country, birthday, favorite_color, hobbies } = this.state;

    const url = getUrl('update_profile');

    const data = JSON.stringify({
      first_name,
      last_name,
      city,
      state,
      country,
      birthday,
      favorite_color,
      hobbies,
    });

    requestPost(url, data)
      .then((res) => {
        this.props.onClickClose();
        this.props.updateUserInfo(res.user_info);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentDidMount() {
    const profileInfo = getProfileInfo(this.props.main);

    for (let i = 0; i < profileInfo.length; i++) {
      const { field, value } = profileInfo[i];
      this.setState({ [field]: value });
    }
  }

  render() {
    const { main, onClickClose } = this.props;
    const profileInfo = getProfileInfo(main);

    return (
      <div styleName="styles.container">
        <div styleName="styles.content">
          {profileInfo.map((item, index) => (
            <div key={index} styleName="styles.line">
              <div styleName="styles.field">{`${item.label}:`}</div>
              <InputFieldProfile
                type="text"
                value={this.state[item.field]}
                name={item.field}
                disabled={item.field === 'email' || item.field === 'created_at'}
                placeholder={item.label}
                maxLength="25"
                onChangeMethod={this.handleFieldChange}
              />
            </div>
          ))}

          <div styleName="styles.buttons">
            <SmallButton label="Save" onClickMethod={this.handleUpdateProfile} />

            <div styleName="shared.button-spacing" />

            <SmallButton label="Cancel" onClickMethod={onClickClose} />
          </div>
        </div>
      </div>
    );
  }
}

export default EditProfile;
