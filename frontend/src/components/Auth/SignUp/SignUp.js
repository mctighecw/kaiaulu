import React from 'react';
import InputField from 'Components/shared/InputField/InputField';
import BigButton from 'Components/shared/BigButton/BigButton';
import '../styles.less';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordReview: '',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleSignUpUser = () => {
    const { firstName, lastName, email, password, passwordReview } = this.state;
    const { history } = this.props;

    if (firstName !== '' && lastName !== '' && email !== '' && password !== '' && passwordReview !== '') {
      if (password !== passwordReview) {
        this.setState({ error: 'Passwords do not match' });
      } else {
        const data = JSON.stringify({
          first_name: firstName,
          last_name: lastName,
          email,
          password,
        });
        this.props
          .signUpUser(data)
          .then((res) => {
            history.push('/login');
          })
          .catch((err) => {
            this.setState({ error: err.status });
          });
      }
    } else {
      this.setState({ error: 'Please fill out all fields' });
    }
  };

  render() {
    const { firstName, lastName, email, password, passwordReview, error } = this.state;
    const { history } = this.props;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="box-left">
            <div styleName="title">
              <span>kaiaulu</span>
              <br />
              social network
            </div>

            <div styleName="auth-fields sign-up">
              <div styleName="heading">sign up</div>

              <InputField
                type="text"
                value={firstName}
                name="firstName"
                placeholder="First Name"
                maxLength="20"
                onChangeMethod={this.handleFieldChange}
              />
              <InputField
                type="text"
                value={lastName}
                name="lastName"
                placeholder="Last Name"
                maxLength="20"
                onChangeMethod={this.handleFieldChange}
              />
              <InputField
                type="text"
                value={email}
                name="email"
                placeholder="Email"
                maxLength="30"
                onChangeMethod={this.handleFieldChange}
              />
              <InputField
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                maxLength="20"
                onChangeMethod={this.handleFieldChange}
              />
              <InputField
                type="password"
                value={passwordReview}
                name="passwordReview"
                placeholder="Retype Password"
                maxLength="20"
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="auth-error">{error}</div>

              <BigButton label="Send" onClickMethod={this.handleSignUpUser} />

              <div styleName="other-link" onClick={() => history.push('/login')}>
                Log in here
              </div>
            </div>
          </div>

          <div styleName="box-right" />
        </div>
      </div>
    );
  }
}

export default SignUp;
