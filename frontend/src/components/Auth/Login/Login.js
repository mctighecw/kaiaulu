import React from 'react';
import { requestGet } from 'Network/requests';
import InputField from 'Components/shared/InputField/InputField';
import BigButton from 'Components/shared/BigButton/BigButton';
import '../styles.less';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'sam@jones.com',
      password: 'sam1',
      loginInfo: {
        user_agent: '',
        language: '',
        ip: '',
        isp: '',
        connection_type: '',
        city: '',
        region: '',
        country: '',
        timezone: '',
      },
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleLoginUser = () => {
    const { email, password, loginInfo } = this.state;

    if (email !== '' && password !== '') {
      this.props.loginUser(email, password, loginInfo);
    } else {
      this.setState({ error: 'Please fill out both fields' });
    }
  };

  handleGetUserInfo = () => {
    return new Promise((resolve, reject) => {
      const { userAgent, language } = navigator;

      this.setState(
        {
          loginInfo: {
            ...this.state.loginInfo,
            user_agent: userAgent,
            language,
          },
        },
        () => {
          resolve();
        }
      );
    });
  };

  handeleGetUserIp = () => {
    return new Promise((resolve, reject) => {
      const url = 'https://json.geoiplookup.io';

      requestGet(url, false)
        .then((res) => {
          this.setState({
            loginInfo: {
              ...this.state.loginInfo,
              ip: res.ip,
              isp: res.isp,
              connection_type: res.connection_type,
              city: res.city,
              region: res.region,
              country: res.country_name,
              timezone: res.timezone_name,
            },
          });
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject();
        });
    });
  };

  getAllInfo = async () => {
    await this.handleGetUserInfo();
    await this.handeleGetUserIp();
  };

  componentDidMount() {
    this.getAllInfo();
  }

  render() {
    const { email, password, error } = this.state;
    const { authError, history } = this.props;
    const errorMessage = error || authError;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="box-left">
            <div styleName="title">
              <span>kaiaulu</span>
              <br />
              social network
            </div>

            <div styleName="auth-fields login">
              <div styleName="heading">login</div>

              <InputField
                type="text"
                value={email}
                name="email"
                placeholder="Email"
                maxLength="30"
                onChangeMethod={this.handleFieldChange}
              />
              <InputField
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                maxLength="20"
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="auth-error">{errorMessage}</div>

              <BigButton label="Send" onClickMethod={this.handleLoginUser} />

              <div styleName="other-link" onClick={() => history.push('/sign-up')}>
                Sign up here
              </div>
            </div>
          </div>

          <div styleName="box-right" />
        </div>
      </div>
    );
  }
}

export default Login;
