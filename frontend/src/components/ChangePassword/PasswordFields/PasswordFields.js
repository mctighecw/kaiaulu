import React, { useState } from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import InputField from 'Components/shared/InputField/InputField';
import SmallButton from 'Components/shared/SmallButton/SmallButton';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

const PasswordFields = ({ main, handleClickChange, handleChangeMessage }) => {
  const [oldPw, setOldPw] = useState('');
  const [newPw, setNewPw] = useState('');
  const [newPwRep, setNewPwRep] = useState('');
  const [error, setError] = useState('');

  const handleSend = () => {
    if (oldPw === '' || newPw === '' || newPwRep === '') {
      setError('All fields are required');
    } else if (newPw !== newPwRep) {
      setError('New passwords do not match');
    } else {
      const url = getUrl('change_password');
      const data = JSON.stringify({
        old_password: oldPw,
        new_password: newPw,
      });

      requestPost(url, data)
        .then((res) => {
          handleChangeMessage('Password changed');
          handleClickChange();
        })
        .catch((err) => {
          console.log(err);
          setError('Incorrect old password');
        });
    }
  };

  return (
    <div styleName="styles.container">
      <div styleName="styles.heading">Change Password</div>

      <InputField
        type="password"
        value={oldPw}
        placeholder="Old password"
        maxLength="15"
        onChangeMethod={(event) => setOldPw(event.target.value)}
      />
      <InputField
        type="password"
        value={newPw}
        placeholder="New password"
        maxLength="15"
        onChangeMethod={(event) => setNewPw(event.target.value)}
      />
      <InputField
        type="password"
        value={newPwRep}
        placeholder="New password (again)"
        maxLength="15"
        onChangeMethod={(event) => setNewPwRep(event.target.value)}
      />

      <div styleName="styles.error">{error}</div>

      <div styleName="styles.buttons">
        <SmallButton label="Send" onClickMethod={handleSend} />
        <div styleName="shared.button-spacing" />
        <SmallButton label="Cancel" onClickMethod={handleClickChange} />
      </div>
    </div>
  );
};

export default PasswordFields;
