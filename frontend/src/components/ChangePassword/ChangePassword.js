import React from 'react';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import PasswordFields from './PasswordFields/PasswordFields';
import './styles.less';

class ChangePassword extends React.Component {
  state = {
    show: 'closed',
    message: '',
  };

  handleClickChange = () => {
    const value = this.state.show === 'closed' ? 'open' : 'closed';
    this.setState({ show: value });
  };

  handleChangeMessage = (message) => {
    this.setState({ message });
  };

  render() {
    const { show, message } = this.state;
    return (
      <div styleName="container">
        {show === 'closed' ? (
          <div>
            <SmallButton
              label="Change password"
              onClickMethod={() => this.setState({ show: 'open', message: '' })}
            />
            <div styleName="message">{message}</div>
          </div>
        ) : (
          <PasswordFields
            handleClickChange={() => this.setState({ show: 'closed' })}
            handleChangeMessage={this.handleChangeMessage}
          />
        )}
      </div>
    );
  }
}

export default ChangePassword;
