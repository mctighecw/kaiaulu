import React from 'react';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import './styles.less';

const Sidebar = ({ history, location }) => {
  const handleChangeLocation = (url) => {
    history.push(`/messages/${url}`);
  };

  const route = history.location.pathname.slice(10);
  const folders = ['inbox', 'sent', 'trash'];

  return (
    <div styleName="container">
      <SmallButton label="New" onClickMethod={() => handleChangeLocation('new')} />

      <div styleName="folders">FOLDERS</div>

      {folders.map((item, index) => (
        <div
          key={index}
          styleName={route === item ? `folder active` : `folder`}
          onClick={() => handleChangeLocation(item)}
        >
          <div styleName="label">{item}</div>
        </div>
      ))}
    </div>
  );
};

export default Sidebar;
