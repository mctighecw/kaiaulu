import React from 'react';
import DisplayMessages from 'Containers/Messages/DisplayMessages';

const Sent = () => (
  <div>
    <DisplayMessages type="sent" label="Sent" />
  </div>
);

export default Sent;
