import React from 'react';
import { getUrl } from 'Network/urls';
import { requestGet, requestPost } from 'Network/requests';
import { formatDateShort } from 'Utils/functions';

import ConfirmModal from 'Components/shared/ConfirmModal/ConfirmModal';
import SmallButton from 'Components/shared/SmallButton/SmallButton';

import MailRead from 'Assets/mail-read.svg';
import MailUnread from 'Assets/mail-unread.svg';
import BoxUp from 'Assets/box-up.svg';
import TrashCan from 'Assets/trash-can.svg';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

class DisplayMessages extends React.Component {
  state = {
    numberMessages: 5,
    sortBy: 'newest',
    filterBy: 'all',
    showModal: false,
    deleteMsg: {
      action: '',
      id: '',
    },
  };

  getUserMessages = () => {
    const { type } = this.props;
    const url = `${getUrl('get_messages')}/${type}`;

    requestGet(url)
      .then((res) => {
        if (type === 'received') this.props.handleUpdateReceivedMessages(res.messages);
        if (type === 'sent') this.props.handleUpdateSentMessages(res.messages);
        if (type === 'deleted') this.props.handleUpdateDeletedMessages(res.messages);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  toggleAllMessages = () => {
    const url = getUrl('toggle_unread');
    const data = JSON.stringify({
      type: 'all',
      message_id: '',
    });

    requestPost(url, data)
      .then((res) => {
        this.getUserMessages();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  toggleMessage = (id) => {
    const url = getUrl('toggle_unread');
    const data = JSON.stringify({
      type: 'single',
      message_id: id,
    });

    requestPost(url, data)
      .then((res) => {
        this.getUserMessages();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deleteMessage = () => {
    const { action, id } = this.state.deleteMsg;
    const url = getUrl('delete_message');
    const type = action === 'delete' ? 'delete' : 'undelete';
    const data = JSON.stringify({ type, message_id: id });

    requestPost(url, data)
      .then((res) => {
        this.getUserMessages();
        this.setState({ showModal: false, deleteMsg: { action: '', id: '' } });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleToggleSortBy = () => {
    const { sortBy } = this.state;
    const value = sortBy === 'newest' ? 'oldest' : 'newest';
    this.setState({ sortBy: value });
  };

  handleToggleFilter = () => {
    const { filterBy } = this.state;
    const value = filterBy === 'all' ? 'unread' : 'all';
    this.setState({ filterBy: value });
  };

  componentDidMount() {
    this.getUserMessages();
  }

  render() {
    const { numberMessages, sortBy, filterBy, showModal, deleteMsg } = this.state;
    const { messages, type, label } = this.props;

    // show type of messages: received, sent, deleted
    const received =
      messages && messages.received && Object.keys(messages.received).length > 0 ? messages.received : {};
    const sent = messages && messages.sent && Object.keys(messages.sent).length > 0 ? messages.sent : {};
    const deleted =
      messages && messages.deleted && Object.keys(messages.deleted).length > 0 ? messages.deleted : {};
    let data;

    if (type === 'received') {
      data = received;
    } else if (type === 'sent') {
      data = sent;
    } else {
      data = deleted;
    }

    // sort by: newest, oldest
    const newest = data && data.length > 0 ? data : [];
    const oldest = newest.length > 0 ? [...newest].reverse() : [];
    const sortedAll = sortBy === 'newest' ? newest : oldest;

    // filter: all, unread
    const unread = sortedAll.length > 0 ? sortedAll.filter((item) => item.unread) : [];
    const filtered = filterBy === 'unread' ? unread : sortedAll;

    // final result
    const displayedMessages = filtered.length > 0 ? filtered.slice(0, numberMessages) : [];

    return (
      <div styleName="styles.container">
        <div styleName="shared.medium-heading">{label}</div>

        {type === 'deleted' && data.length > 0 && (
          <div styleName="styles.sub-heading">
            These messages are in the trash. Click below to delete them permanently.
          </div>
        )}

        {data.length > 0 && (
          <div styleName="styles.sort-by">
            <div styleName="styles.item">
              Sort by: <span onClick={this.handleToggleSortBy}>{`${sortBy} first`}</span>
            </div>

            {type === 'received' && (
              <div styleName="styles.item">
                Filter: <span onClick={this.handleToggleFilter}>{filterBy}</span>
              </div>
            )}

            {type === 'received' && unread.length > 0 && (
              <div styleName="styles.link" onClick={() => this.toggleAllMessages()}>
                Mark all as read
              </div>
            )}
          </div>
        )}

        <div styleName="styles.message-box">
          {displayedMessages.length > 0 ? (
            displayedMessages.map((item, index) => {
              const toFrom = type === 'sent' ? `To: ${item.to}` : `From: ${item.from}`;
              const label = type === 'sent' ? 'Sent' : 'Received';
              const date = `${label} ${formatDateShort(item.created_at)}`;
              const unreadIcon = item.unread ? MailUnread : MailRead;
              const unreadLabel = item.unread ? 'Unread' : 'Read';

              return (
                <div key={index} styleName="styles.message">
                  <div key={index} styleName="styles.right-icons">
                    {type === 'received' && (
                      <img
                        src={unreadIcon}
                        alt={unreadLabel}
                        title={unreadLabel}
                        onClick={() => this.toggleMessage(item.id)}
                      />
                    )}
                    {type === 'deleted' && (
                      <img
                        src={BoxUp}
                        alt="Undelete"
                        title="Undelete"
                        onClick={() =>
                          this.setState({ showModal: true, deleteMsg: { action: 'undelete', id: item.id } })
                        }
                      />
                    )}
                    {(type === 'received' || type === 'deleted') && (
                      <img
                        src={TrashCan}
                        alt="Delete"
                        title="Delete"
                        onClick={() =>
                          this.setState({ showModal: true, deleteMsg: { action: 'delete', id: item.id } })
                        }
                      />
                    )}
                  </div>
                  <div styleName="styles.subject">{item.subject}</div>
                  <div styleName="styles.to-from-date">{`${toFrom} · ${date}`}</div>
                  <div styleName="styles.body">{item.body}</div>
                </div>
              );
            })
          ) : (
            <div styleName="styles.no-messages">No messages</div>
          )}

          {numberMessages < sortedAll.length && (
            <div styleName="styles.more-button">
              <SmallButton
                label="Show more"
                onClickMethod={() => this.setState({ numberMessages: numberMessages + 5 })}
              />
            </div>
          )}
        </div>

        {showModal && (
          <ConfirmModal
            action={deleteMsg.action}
            type="message"
            onClickCancel={() => this.setState({ showModal: false, deleteMsg: { action: '', id: '' } })}
            onClickConfirm={this.deleteMessage}
          />
        )}
      </div>
    );
  }
}

export default DisplayMessages;
