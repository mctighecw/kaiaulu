import React from 'react';
import DisplayMessages from 'Containers/Messages/DisplayMessages';

const Inbox = () => (
  <div>
    <DisplayMessages type="received" label="Inbox" />
  </div>
);

export default Inbox;
