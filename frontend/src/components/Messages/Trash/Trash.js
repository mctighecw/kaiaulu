import React from 'react';
import DisplayMessages from 'Containers/Messages/DisplayMessages';

const Trash = () => (
  <div>
    <DisplayMessages type="deleted" label="Trash" />
  </div>
);

export default Trash;
