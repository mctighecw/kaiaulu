import React from 'react';
import { getUrl } from 'Network/urls';
import { requestGet, requestPost } from 'Network/requests';

import DropDown from 'Components/shared/DropDown/DropDown';
import InputFieldLarge from 'Components/shared/InputFieldLarge/InputFieldLarge';
import TextBox from 'Components/shared/TextBox/TextBox';
import SmallButton from 'Components/shared/SmallButton/SmallButton';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

class New extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      to: '',
      subject: '',
      body: '',
      error: '',
      success: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleSelectChange = (event) => {
    const { value } = event.target;
    this.setState({ to: value });
  };

  handleSendMessage = () => {
    const { to, subject, body } = this.state;
    let error = '';

    this.setState({ error: '', success: '' });

    if (to === '') {
      error = 'Please select a recipient';
    } else if (subject === '' || body === '') {
      error = 'Please fill out both fields';
    }

    if (error !== '') {
      this.setState({ error });
    } else {
      const url = getUrl('new_message');
      const data = JSON.stringify({
        to_user: to,
        subject,
        body,
      });

      requestPost(url, data)
        .then((res) => {
          this.handleClear();
          this.setState({ success: 'Message has been sent' });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleClear = () => {
    this.setState({
      to: '',
      subject: '',
      body: '',
      error: '',
      success: '',
    });
  };

  getAllUsers = () => {
    const url = getUrl('get_all_user_list');

    requestGet(url)
      .then((res) => {
        this.props.handleSaveAllUsers(res.all_users);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentDidMount() {
    this.getAllUsers();
  }

  render() {
    const { to, subject, body, error, success } = this.state;
    const { messages } = this.props;
    let options = [{ value: '', label: 'To' }];

    for (let i = 0; i < messages.users.length; i++) {
      const { id, name } = messages.users[i];
      options.push({ value: id, label: name });
    }

    return (
      <div styleName="styles.container">
        <div styleName="shared.medium-heading">New Message</div>

        <div styleName="styles.new-message">
          <DropDown value={to} placeHolder="To" options={options} onChangeMethod={this.handleSelectChange} />
          <InputFieldLarge
            type="text"
            value={subject}
            name="subject"
            placeholder="Subject"
            maxLength="40"
            onChangeMethod={this.handleFieldChange}
          />
          <TextBox
            rows="10"
            name="body"
            placeholder="Message"
            value={body}
            maxLength="2000"
            onChangeMethod={this.handleFieldChange}
          />

          <div styleName="styles.status">
            {error !== '' && <div styleName="styles.error">{error}</div>}
            {success !== '' && <div styleName="styles.success">{success}</div>}
          </div>

          <SmallButton label="Send" onClickMethod={this.handleSendMessage} />

          <div styleName="shared.button-spacing" />

          <SmallButton label="Clear" onClickMethod={this.handleClear} />
        </div>
      </div>
    );
  }
}

export default New;
