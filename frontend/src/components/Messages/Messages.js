import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import './styles.less';

import Sidebar from './Sidebar/Sidebar';
import New from 'Containers/Messages/New';
import Inbox from './Inbox/Inbox';
import Sent from './Sent/Sent';
import Trash from './Trash/Trash';

class Messages extends React.Component {
  render() {
    const { history, location, match } = this.props;

    return (
      <div styleName="container">
        <Sidebar history={history} location={location} />

        <Switch>
          <Route exact path={`${match.path}`} render={() => <Redirect to="/messages/inbox" />} />
          <Route exact path={`${match.path}/new`} component={New} />
          <Route exact path={`${match.path}/inbox`} component={Inbox} />
          <Route path={`${match.path}/sent`} component={Sent} />
          <Route path={`${match.path}/trash`} component={Trash} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(Messages);
