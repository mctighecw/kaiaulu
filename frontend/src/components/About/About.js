import React from 'react';
import styles from './styles.less';
import shared from '../../styles/shared.less';

import ReactImage from 'Assets/tech/react.svg';
import ReduxImage from 'Assets/tech/redux.svg';
import PythonImage from 'Assets/tech/python.svg';
import PostgresImage from 'Assets/tech/postgres.svg';

const About = () => {
  const technologies = [
    {
      name: 'React',
      image: ReactImage,
      link: 'reactjs.org',
    },
    {
      name: 'Redux',
      image: ReduxImage,
      link: 'redux.js.org',
    },
    {
      name: 'Python Flask',
      image: PythonImage,
      link: 'flask.pocoo.org',
    },
    {
      name: 'PostgreSQL',
      image: PostgresImage,
      link: 'www.postgresql.org',
    },
  ];

  const openLink = (url) => {
    const path = `http://${url}`;
    window.open(path);
  };

  return (
    <div styleName="styles.container">
      <div styleName="styles.content">
        <div styleName="shared.big-heading">About</div>

        <div styleName="styles.about">
          <p>
            <span>kaiaulu</span> is the Hawaiian word for "community".
          </p>
          <p>
            This is an online community where you can share your profile, write blog posts, upload pictures,
            send messages, and meet new people.
          </p>
          <p>We only save what data you submit &#8212; no spying or sneakiness, we promise.</p>
          <p>
            Enjoy your stay at <span>kaiaulu</span>! &#128522;
          </p>
        </div>

        <div styleName="shared.big-heading">Tech Stack</div>

        <div styleName="styles.tech-stack">
          <p>
            For those interested, here are some of the technologies used to build <span>kaiaulu</span>:
          </p>

          {technologies.map((item, index) => (
            <div key={index} styleName="styles.item">
              <img src={item.image} alt={item.name} />
              <div styleName="styles.name" onClick={() => openLink(item.link)}>
                {item.name}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default About;
