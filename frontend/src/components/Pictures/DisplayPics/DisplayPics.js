import React from 'react';
import { getUrl } from 'Network/urls';
import { requestGet, requestPost } from 'Network/requests';
import { formatDateShort } from 'Utils/functions';
import { Image, CloudinaryContext } from 'cloudinary-react';
import PicModal from '../PicModal/PicModal';
import ConfirmModal from 'Components/shared/ConfirmModal/ConfirmModal';

import ThumbUpIcon from 'Assets/thumb-up.svg';
import TrashCan from 'Assets/trash-can.svg';
import './styles.less';

const cloudName = process.env.CLOUD_NAME;

class DisplayPics extends React.Component {
  state = {
    data: [],
    likes: {},
    showPicModal: false,
    showConfirmModal: false,
    modalData: {},
    id: '',
  };

  getPictures = () => {
    const { type } = this.props;
    const url = `${getUrl('get_pictures')}/${type}`;

    requestGet(url)
      .then((res) => {
        this.setState({ data: res.pictures });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLikePicture = (id, likes) => {
    const url = getUrl('like_picture');
    const data = JSON.stringify({ id });

    requestPost(url, data)
      .then((res) => {
        // update likes without refetching data
        this.setState({
          likes: {
            ...this.state.likes,
            [id]: likes + 1,
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleDeletePicture = () => {
    const { id } = this.state;
    const url = getUrl('delete_picture');
    const data = JSON.stringify({ id });

    requestPost(url, data)
      .then((res) => {
        // modify data locally
        this.setState({ showConfirmModal: false, id: '' });
        this.getPictures();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentDidMount() {
    this.getPictures();
  }

  render() {
    const { userInfo } = this.props.main;
    const { data, likes, showPicModal, showConfirmModal, modalData } = this.state;

    return (
      <div styleName="container">
        <CloudinaryContext cloudName={cloudName}>
          <div styleName="image-container">
            {data.length > 0 ? (
              data.map((item, index) => {
                const dateCreated = formatDateShort(item.created_at);
                const likesNr = likes[item.id] ? likes[item.id] : item.likes;

                return (
                  <div key={index} styleName="box">
                    <Image
                      publicId={item.pic_url}
                      crop="scale"
                      styleName="image"
                      onClick={() => this.setState({ showPicModal: true, modalData: item })}
                    />
                    <div styleName="caption">{item.caption}</div>
                    <div styleName="user">
                      {`Uploaded by ${item.user_name}`}
                      <br />
                      {`on ${dateCreated}`}
                    </div>

                    <div styleName="like-row">
                      <div styleName="likes">{likesNr}</div>
                      <img
                        src={ThumbUpIcon}
                        alt="Like this"
                        styleName="thumb-up"
                        onClick={() => this.handleLikePicture(item.id, likesNr)}
                      />
                      {(userInfo.id === item.user_id || userInfo.is_admin) && (
                        <img
                          src={TrashCan}
                          alt="Delete"
                          title="Delete"
                          styleName="trash-can"
                          onClick={() => this.setState({ showConfirmModal: true, id: item.id })}
                        />
                      )}
                    </div>
                  </div>
                );
              })
            ) : (
              <div styleName="no-pics">No pictures</div>
            )}
          </div>
        </CloudinaryContext>

        {showPicModal && (
          <PicModal
            modalData={modalData}
            onClickClose={() => this.setState({ showPicModal: false, modalData: {} })}
          />
        )}

        {showConfirmModal && (
          <ConfirmModal
            action="delete"
            type="picture"
            onClickCancel={() => this.setState({ showConfirmModal: false, id: '' })}
            onClickConfirm={this.handleDeletePicture}
          />
        )}
      </div>
    );
  }
}

export default DisplayPics;
