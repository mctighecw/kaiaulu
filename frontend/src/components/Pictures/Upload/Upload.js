import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import InputFieldLarge from 'Components/shared/InputFieldLarge/InputFieldLarge';
import { UPLOAD_SUCCESS, UPLOAD_ERROR, UPLOAD_NOT_ALLOWED } from 'Constants/messages';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

const Upload = () => {
  const [caption, setCaption] = useState('');
  const [status, setStatus] = useState([{ type: '', message: '' }]);

  const uploadPicture = (data) => {
    const url = getUrl('upload_picture');

    requestPost(url, data, false)
      .then((res) => {
        setCaption('');
        setStatus({ type: 'success', message: UPLOAD_SUCCESS });
      })
      .catch((err) => {
        console.log(err);
        setStatus({ type: 'error', message: UPLOAD_ERROR });
      });
  };

  const onDrop = (files) => {
    const multipleFiles = files.length === 0;

    if (multipleFiles) {
      setStatus({ type: 'error', message: UPLOAD_NOT_ALLOWED });
    } else {
      const formData = new FormData();
      formData.append('file', files[0]);
      formData.append('type', 'general');
      formData.append('caption', caption);
      uploadPicture(formData);
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/jpeg, image/png, image/gif',
    multiple: false,
    onDrop,
  });

  return (
    <div styleName="styles.container">
      <div styleName="shared.medium-heading">Upload</div>

      <div styleName="styles.caption-box">
        <InputFieldLarge
          type="text"
          value={caption}
          name="caption"
          placeholder="Photo caption"
          onChangeMethod={(event) => setCaption(event.target.value)}
        />
      </div>

      <div styleName="styles.drop-zone">
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          {isDragActive ? <p>Drop your picture here</p> : <p>Click to select a picture or drag one here</p>}
        </div>
      </div>

      <div styleName="styles.directions">
        Please upload one file at a time.
        <br />
        Allowed file types are jpg/jpeg, png, gif.
      </div>

      <div styleName={`styles.status ${status.type ? `styles.${status.type}` : ''}`}>{status.message}</div>
    </div>
  );
};

export default Upload;
