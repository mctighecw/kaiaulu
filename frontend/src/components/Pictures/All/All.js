import React from 'react';
import DisplayPics from 'Containers/DisplayPics';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

const All = () => {
  return (
    <div styleName="styles.container">
      <div styleName="shared.medium-heading">All Pictures</div>

      <DisplayPics type="all" />
    </div>
  );
};

export default All;
