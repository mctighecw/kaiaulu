import React, { useCallback } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import './styles.less';

import Sidebar from './Sidebar/Sidebar';
import Upload from './Upload/Upload';
import All from './All/All';
import Mine from './Mine/Mine';

const Pictures = (props) => {
  const { history, location, match } = props;

  return (
    <div styleName="container">
      <Sidebar history={history} location={location} />

      <Switch>
        <Route exact path={`${match.path}`} render={() => <Redirect to="/pictures/all" />} />
        <Route exact path={`${match.path}/upload`} component={Upload} />
        <Route exact path={`${match.path}/all`} component={All} />
        <Route exact path={`${match.path}/mine`} component={Mine} />
      </Switch>
    </div>
  );
};

export default withRouter(Pictures);
