import React from 'react';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import './styles.less';

const Sidebar = ({ history, location }) => {
  const handleChangeLocation = (url) => {
    history.push(`/pictures/${url}`);
  };

  const route = history.location.pathname.slice(10);
  const folders = ['all', 'mine'];

  return (
    <div styleName="container">
      <SmallButton label="Upload" onClickMethod={() => handleChangeLocation('upload')} />

      <div styleName="albums">ALBUMS</div>

      {folders.map((item, index) => (
        <div
          key={index}
          styleName={route === item ? `folder active` : `folder`}
          onClick={() => handleChangeLocation(item)}
        >
          <div styleName="label">{item}</div>
        </div>
      ))}
    </div>
  );
};

export default Sidebar;
