import React from 'react';
import DisplayPics from 'Containers/DisplayPics';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

class Mine extends React.Component {
  render() {
    return (
      <div styleName="styles.container">
        <div styleName="shared.medium-heading">My Pictures</div>

        <DisplayPics type="user" />
      </div>
    );
  }
}

export default Mine;
