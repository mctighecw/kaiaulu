import React from 'react';
import { Image, CloudinaryContext } from 'cloudinary-react';
import { formatDateShort } from 'Utils/functions';

import SmallButton from 'Components/shared/SmallButton/SmallButton';
import CloseIcon from 'Assets/close-icon.svg';
import './styles.less';

const cloudName = process.env.CLOUD_NAME;

const PicModal = ({ modalData, onClickClose }) => {
  const dateCreated = formatDateShort(modalData.created_at);

  return (
    <div styleName="modal-background">
      <div styleName="box">
        <img src={CloseIcon} alt="" styleName="close" onClick={onClickClose} />

        <div styleName="content">
          <CloudinaryContext cloudName={cloudName}>
            <Image publicId={modalData.pic_url} crop="scale" styleName="image" />
          </CloudinaryContext>
          <div styleName="info">
            {`${modalData.caption} | Uploaded by ${modalData.user_name} on ${dateCreated}`}
          </div>
          <div styleName="button">
            <SmallButton label="Close" onClickMethod={onClickClose} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PicModal;
