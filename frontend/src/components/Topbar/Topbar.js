import React from 'react';
import links from 'Constants/links';
import './styles.less';

class Topbar extends React.Component {
  handleLogout = () => {
    const { history } = this.props;
    this.props.logoutUser();
  };

  render() {
    const { history, match } = this.props;

    return (
      <div styleName="container">
        <div styleName="title">kaiaulu</div>

        <div styleName="right-box">
          {links.map((item, index) => {
            const style = match.path === item.url ? `link active` : `link`;
            return (
              <div key={index} styleName={style} onClick={() => history.push(item.url)}>
                {item.link}
              </div>
            );
          })}

          <div styleName="link" onClick={this.handleLogout}>
            Logout
          </div>
        </div>
      </div>
    );
  }
}

export default Topbar;
