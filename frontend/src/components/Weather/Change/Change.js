import React, { useState } from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import InputField from 'Components/shared/InputField/InputField';
import SmallButton from 'Components/shared/SmallButton/SmallButton';

import shared from '../../../styles/shared.less';
import styles from './styles.less';

const Change = ({ dashboard, handleClickChange, handleUpdateWeather }) => {
  const [city, setCity] = useState('');
  const [country, setCountry] = useState('');

  const handleSend = () => {
    if (city !== '' && country !== '') {
      const url = getUrl('request_weather_info');
      const data = JSON.stringify({
        city,
        country,
      });

      requestPost(url, data)
        .then((res) => {
          if (res.weather) {
            handleUpdateWeather(res.weather);
          }
          handleClickChange();
        })
        .catch((err) => {
          console.log(err);
          handleClickChange();
        });
    }
  };

  const handleCancel = () => {
    handleClickChange();
  };

  return (
    <div styleName="styles.container">
      <div styleName="styles.location">New weather location</div>

      <InputField
        type="text"
        value={city}
        placeholder="City"
        maxLength="15"
        onChangeMethod={(event) => setCity(event.target.value)}
      />
      <InputField
        type="text"
        value={country}
        placeholder="Country"
        maxLength="15"
        onChangeMethod={(event) => setCountry(event.target.value)}
      />

      <div styleName="styles.buttons">
        <SmallButton label="Send" onClickMethod={handleSend} />
        <div styleName="shared.button-spacing" />
        <SmallButton label="Cancel" onClickMethod={handleCancel} />
      </div>
    </div>
  );
};

export default Change;
