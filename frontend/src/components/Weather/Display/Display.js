import React from 'react';
import './styles.less';

const Display = ({ dashboard, handleClickChange }) => {
  const roundNumber = (nr) => {
    return Math.round(nr);
  };

  const currentTemp =
    dashboard.weather.temp && dashboard.weather.temp.temp ? roundNumber(dashboard.weather.temp.temp) : 'NA';
  const maxTemp =
    dashboard.weather.temp && dashboard.weather.temp.max_temp
      ? roundNumber(dashboard.weather.temp.max_temp)
      : 'NA';
  const minTemp =
    dashboard.weather.temp && dashboard.weather.temp.min_temp
      ? roundNumber(dashboard.weather.temp.min_temp)
      : 'NA';

  return (
    <div styleName="container">
      {dashboard.weather.location !== '' ? (
        <div>
          <div styleName="location">
            Current weather in <span>{dashboard.weather.location}</span>
          </div>
          <div styleName="info">{`${dashboard.weather.general} (${dashboard.weather.description})`}</div>
          <div>
            <div styleName="temp">Temperatures:</div>
            <div styleName="line">{`Current: ${currentTemp}° C`}</div>
            <div styleName="line">{`High: ${maxTemp}° C`}</div>
            <div styleName="line">{`Low: ${minTemp}° C`}</div>
          </div>
        </div>
      ) : (
        <div styleName="no-info">
          No weather data available.
          <br />
          Please select a location below.
        </div>
      )}
      <div styleName="change" onClick={handleClickChange}>
        Change location
      </div>
    </div>
  );
};

export default Display;
