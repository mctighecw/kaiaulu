import React from 'react';
import Display from './Display/Display';
import Change from './Change/Change';
import './styles.less';

class Weather extends React.Component {
  state = {
    show: 'display',
  };

  handleClickChange = () => {
    const { show } = this.state;
    const value = show === 'display' ? 'change' : 'display';
    this.setState({ show: value });
  };

  render() {
    const { dashboard, handleUpdateWeather } = this.props;
    const { show } = this.state;

    return (
      <div styleName="container">
        {show === 'display' ? (
          <Display dashboard={dashboard} handleClickChange={this.handleClickChange} />
        ) : (
          <Change
            dashboard={dashboard}
            handleClickChange={this.handleClickChange}
            handleUpdateWeather={handleUpdateWeather}
          />
        )}
      </div>
    );
  }
}

export default Weather;
