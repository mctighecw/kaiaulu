import * as types from 'Actions/actionTypes';

const initialState = {
  allPosts: [],
  userPosts: [],
  filter: {
    numberPosts: 5,
    sortBy: 'newest',
    searchTerm: '',
  },
};

const posts = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_ALL_POSTS:
      return {
        ...state,
        allPosts: action.data,
      };
    case types.UPDATE_USER_POSTS:
      return {
        ...state,
        userPosts: action.data,
      };
    case types.UPDATE_FILTER:
      return {
        ...state,
        filter: {
          ...state.filter,
          ...action.data,
        },
      };
    default:
      return state;
  }
};

export default posts;
