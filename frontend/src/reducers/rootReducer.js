import { combineReducers } from 'redux';

import auth from './auth';
import dashboard from './dashboard';
import main from './main';
import messages from './messages';
import posts from './posts';

const rootReducer = combineReducers({
  auth,
  dashboard,
  main,
  messages,
  posts,
});

export default rootReducer;
