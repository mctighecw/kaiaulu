import * as types from 'Actions/actionTypes';

const initialState = {
  counts: {
    users: 0,
    posts: 0,
    messages: 0,
    pictures: 0,
  },
  newPics: 0,
  recentPosts: [],
  weather: {
    location: '',
    general: '',
    description: '',
    temp: {
      temp: '',
      max_temp: '',
      min_temp: '',
    },
  },
};

const dashboard = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_COUNTS:
      return {
        ...state,
        counts: {
          ...action.data,
        },
      };
    case types.UPDATE_NEW_PIC_COUNT:
      return {
        ...state,
        newPics: action.value,
      };
    case types.UPDATE_RECENT_POSTS:
      return {
        ...state,
        recentPosts: [...action.data],
      };
    case types.UPDATE_WEATHER:
      return {
        ...state,
        weather: { ...action.data },
      };
    default:
      return state;
  }
};

export default dashboard;
