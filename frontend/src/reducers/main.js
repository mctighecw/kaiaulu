import * as types from 'Actions/actionTypes';

const initialState = {
  userInfo: {
    id: 0,
    email: '',
    first_name: '',
    last_name: '',
    city: '',
    state: '',
    country: '',
    birthday: '',
    favorite_color: '',
    hobbies: '',
    created_at: '',
    is_admin: false,
    pic_url: '',
  },
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER_INFO:
      return {
        ...state,
        userInfo: {
          ...state.userInfo,
          ...action.data,
        },
      };
    case types.CLEAR_USER_INFO:
      return {
        ...state,
        userInfo: {
          ...initialState.userInfo,
        },
      };
    default:
      return state;
  }
};

export default main;
