import * as types from 'Actions/actionTypes';

const initialState = {
  users: [],
  received: [],
  sent: [],
  deleted: [],
  unreadCount: 0,
};

const messages = (state = initialState, action) => {
  switch (action.type) {
    case types.SAVE_ALL_USERS:
      return {
        ...state,
        users: action.data,
      };
    case types.UPDATE_RECEIVED_MESSAGES:
      return {
        ...state,
        received: action.data,
      };
    case types.UPDATE_SENT_MESSAGES:
      return {
        ...state,
        sent: action.data,
      };
    case types.UPDATE_DELETED_MESSAGES:
      return {
        ...state,
        deleted: action.data,
      };
    case types.UPDATE_UNREAD_COUNT:
      return {
        ...state,
        unreadCount: action.value,
      };
    default:
      return state;
  }
};

export default messages;
