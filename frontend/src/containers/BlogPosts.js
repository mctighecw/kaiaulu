import { connect } from 'react-redux';
import { updateAllPosts, updateUserPosts, updatePostsFilter } from 'Actions/postActions';
import BlogPosts from 'Components/BlogPosts/BlogPosts';

const mapStateToProps = (state) => ({
  main: state.main,
  posts: state.posts,
});

const mapDispatchToProps = (dispatch) => ({
  handleUpdateAllPosts: (data) => dispatch(updateAllPosts(data)),
  handleUpdateUserPosts: (data) => dispatch(updateUserPosts(data)),
  handleUpdatePostsFilter: (data) => dispatch(updatePostsFilter(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlogPosts);
