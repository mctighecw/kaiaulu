import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logout } from 'Services/Auth';
import Topbar from 'Components/Topbar/Topbar';

const mapDispatchToProps = (dispatch) => ({
  logoutUser: () => logout(),
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(Topbar)
);
