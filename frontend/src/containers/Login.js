import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { login } from 'Services/Auth';
import LoginPage from 'Pages/Login';

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  loginUser: (email, password, loginInfo) => login(email, password, loginInfo),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(LoginPage)
);
