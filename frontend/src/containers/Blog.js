import { connect } from 'react-redux';
import { updateAllPosts } from 'Actions/postActions';
import Blog from 'Components/Blog/Blog';

const mapStateToProps = (state) => ({
  posts: state.posts,
});

const mapDispatchToProps = (dispatch) => ({
  handleUpdateAllPosts: (data) => dispatch(updateAllPosts(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog);
