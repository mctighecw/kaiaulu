import { connect } from 'react-redux';
import { updateReceivedMessages, updateSentMessages, updateDeletedMessages } from 'Actions/messageActions';
import DisplayMessages from 'Components/Messages/DisplayMessages/DisplayMessages';

const mapStateToProps = (state) => ({
  messages: state.messages,
});

const mapDispatchToProps = (dispatch) => ({
  handleUpdateReceivedMessages: (data) => dispatch(updateReceivedMessages(data)),
  handleUpdateSentMessages: (data) => dispatch(updateSentMessages(data)),
  handleUpdateDeletedMessages: (data) => dispatch(updateDeletedMessages(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DisplayMessages);
