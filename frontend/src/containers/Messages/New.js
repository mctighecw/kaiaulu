import { connect } from 'react-redux';
import { saveAllUsers } from 'Actions/messageActions';
import New from 'Components/Messages/New/New';

const mapStateToProps = (state) => ({
  messages: state.messages,
});

const mapDispatchToProps = (dispatch) => ({
  handleSaveAllUsers: (data) => dispatch(saveAllUsers(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(New);
