import { connect } from 'react-redux';
import { updateWeather } from 'Actions/dashboardActions';
import Weather from 'Components/Weather/Weather';

const mapStateToProps = (state) => ({
  dashboard: state.dashboard,
});

const mapDispatchToProps = (dispatch) => ({
  handleUpdateWeather: (data) => dispatch(updateWeather(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Weather);
