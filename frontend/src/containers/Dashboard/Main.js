import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Main from 'Components/Dashboard/Main/Main';

const mapStateToProps = (state) => ({
  main: state.main,
  dashboard: state.dashboard,
  messages: state.messages,
});

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Main)
);
