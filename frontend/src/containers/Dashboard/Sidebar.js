import { connect } from 'react-redux';
import Sidebar from 'Components/Dashboard/Sidebar/Sidebar';

const mapStateToProps = (state) => ({
  dashboard: state.dashboard,
});

export default connect(
  mapStateToProps,
  null
)(Sidebar);
