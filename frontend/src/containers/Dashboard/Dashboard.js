import { connect } from 'react-redux';
import { updateCounts, updateNewPicCount, updateRecentPosts, updateWeather } from 'Actions/dashboardActions';
import { updateUnreadCount } from 'Actions/messageActions';
import Dashboard from 'Components/Dashboard/Dashboard';

const mapDispatchToProps = (dispatch) => ({
  handleUpdateCounts: (data) => dispatch(updateCounts(data)),
  handleUpdateUnreadCount: (value) => dispatch(updateUnreadCount(value)),
  handleUpdateNewPicCount: (value) => dispatch(updateNewPicCount(value)),
  handleUpdateRecentPosts: (data) => dispatch(updateRecentPosts(data)),
  handleUpdateWeather: (data) => dispatch(updateWeather(data)),
});

export default connect(
  null,
  mapDispatchToProps
)(Dashboard);
