import { connect } from 'react-redux';
import { setUserInfo } from 'Actions/mainActions';
import Profile from 'Components/Profile/Profile';

const mapStateToProps = (state) => ({
  main: state.main,
});

const mapDispatchToProps = (dispatch) => ({
  updateUserInfo: (data) => dispatch(setUserInfo(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
