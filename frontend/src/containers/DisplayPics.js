import { connect } from 'react-redux';
import DisplayPics from 'Components/Pictures/DisplayPics/DisplayPics';

const mapStateToProps = (state) => ({
  main: state.main,
});

export default connect(
  mapStateToProps,
  null
)(DisplayPics);
