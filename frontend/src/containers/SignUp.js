import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { register } from 'Services/Auth';
import SignUpPage from 'Pages/SignUp';

const mapDispatchToProps = (dispatch) => ({
  signUpUser: (data) => register(data),
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(SignUpPage)
);
