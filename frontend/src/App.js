import React, { Suspense } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { checkAuthentication } from 'Services/Auth';

import Loading from 'Components/Loading/Loading';
const Login = React.lazy(() => import('./containers/Login'));
const SignUp = React.lazy(() => import('./containers/SignUp'));
const MainRoutes = React.lazy(() => import('./MainRoutes'));

import './styles/global.css';

class App extends React.Component {
  checkAuthStatus = () => {
    checkAuthentication()
      .then((res) => {
        if (res.auth) {
          const { pathname } = this.props.location;
          const path = pathname === '/login' ? '/' : pathname;
          this.props.history.push(path);
        } else {
          this.props.history.push('/login');
        }
      })
      .catch((err) => {
        this.props.history.push('/login');
      });
  };

  componentWillMount() {
    this.checkAuthStatus();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.auth !== this.props.auth) {
      this.checkAuthStatus();
    }
  }

  render() {
    return (
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route path="/login" render={() => <Login />} />
          <Route path="/sign-up" render={() => <SignUp />} />
          <Route path="/" render={() => <MainRoutes />} />
        </Switch>
      </Suspense>
    );
  }
}

export default withRouter(App);
