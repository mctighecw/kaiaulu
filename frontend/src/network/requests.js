import axios from 'axios';

export const requestGet = (url, credentials = true) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'GET',
      url,
      headers: { 'Content-type': 'application/x-www-form-urlencoded' },
      withCredentials: credentials,
      responseType: 'json',
    })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err.response);
      });
  });
};

export const requestPost = (url, data, formEncoded = true) => {
  const headers = formEncoded ? { 'Content-type': 'application/x-www-form-urlencoded' } : {};
  return new Promise((resolve, reject) => {
    axios({
      method: 'POST',
      url,
      headers,
      withCredentials: true,
      responseType: 'json',
      data,
    })
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err.response);
      });
  });
};
