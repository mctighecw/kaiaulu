const getBaseUrl = () => {
  const dev = process.env.NODE_ENV === 'development';

  if (dev) {
    return 'http://localhost:9000';
  } else {
    return '';
  }
};

export const getUrl = (type) => {
  const baseUrl = getBaseUrl();

  switch (type) {
    // Auth
    case 'register':
      return `${baseUrl}/api/auth/register`;
    case 'login':
      return `${baseUrl}/api/auth/login`;
    case 'check_auth':
      return `${baseUrl}/api/auth/check`;
    case 'change_password':
      return `${baseUrl}/api/auth/change_password`;
    case 'logout':
      return `${baseUrl}/api/auth/logout`;

    // User
    case 'get_counts':
      return `${baseUrl}/api/user/get_counts`;
    case 'update_profile':
      return `${baseUrl}/api/user/update_profile`;
    case 'get_users':
      return `${baseUrl}/api/user/get_users`;
    case 'get_weather_info':
      return `${baseUrl}/api/user/get_weather_info`;
    case 'request_weather_info':
      return `${baseUrl}/api/user/request_weather_info`;

    // Posts
    case 'get_blog_posts':
      return `${baseUrl}/api/post/get_blog_posts`;
    case 'get_recent_posts':
      return `${baseUrl}/api/post/get_recent_posts`;
    case 'create_post':
      return `${baseUrl}/api/post/create_post`;
    case 'delete_post':
      return `${baseUrl}/api/post/delete_post`;

    // Messages
    case 'get_all_user_list':
      return `${baseUrl}/api/message/get_all_user_list`;
    case 'get_messages':
      return `${baseUrl}/api/message/get_messages`;
    case 'new_message':
      return `${baseUrl}/api/message/new_message`;
    case 'toggle_unread':
      return `${baseUrl}/api/message/toggle_unread`;
    case 'delete_message':
      return `${baseUrl}/api/message/delete_message`;
    case 'get_unread_number':
      return `${baseUrl}/api/message/get_unread_number`;

    // Pictures
    case 'upload_picture':
      return `${baseUrl}/api/picture/upload_picture`;
    case 'get_pictures':
      return `${baseUrl}/api/picture/get_pictures`;
    case 'like_picture':
      return `${baseUrl}/api/picture/like_picture`;
    case 'delete_picture':
      return `${baseUrl}/api/picture/delete_picture`;
    case 'new_picture_count':
      return `${baseUrl}/api/picture/new_picture_count`;

    default:
      return baseUrl;
  }
};
