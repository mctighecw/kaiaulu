import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Dashboard from 'Pages/Dashboard';
import Blog from 'Pages/Blog';
import Pictures from 'Pages/Pictures';
import Messages from 'Pages/Messages';
import Users from 'Pages/Users';
import Profile from 'Pages/Profile';
import About from 'Pages/About';
import Page404 from 'Pages/Page404';

const MainRoutes = () => (
  <Switch>
    <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
    <Route path="/dashboard" component={Dashboard} />
    <Route path="/blog" component={Blog} />
    <Route path="/pictures" component={Pictures} />
    <Route path="/messages" component={Messages} />
    <Route path="/users" component={Users} />
    <Route path="/profile" component={Profile} />
    <Route path="/about" component={About} />
    <Route component={Page404} />
  </Switch>
);

export default MainRoutes;
