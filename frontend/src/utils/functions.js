import moment from 'moment';

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

export const formatDateOnly = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('MMMM Do, YYYY');
};

export const formatDateShort = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('MM.DD.YYYY, HH:mm');
};

export const formatDateLong = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('MMMM Do, YYYY, h:mm a');
};

export const formatDateFromNow = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime, 'YYYY-MM-DD hh:mm:ss +ZZ').fromNow();
};
