import { formatDateOnly } from 'Utils/functions';

const getUserInfo = (user) => {
  return [
    {
      label: 'Name',
      value: `${user.first_name} ${user.last_name}`,
    },
    {
      label: 'Location',
      value: `${user.city} ${user.state} ${user.country}`,
    },
    {
      label: 'Birthday',
      value: user.birthday,
    },
    {
      label: 'Favorite color',
      value: user.favorite_color,
    },
    {
      label: 'Hobbies',
      value: user.hobbies,
    },
    {
      label: 'Member since',
      value: formatDateOnly(user.created_at),
    },
  ];
};

export default getUserInfo;
