export const UPLOAD_SUCCESS = 'File uploaded successfully';
export const UPLOAD_ERROR = 'Error - please try again';
export const UPLOAD_NOT_ALLOWED = 'Error - not allowed';
