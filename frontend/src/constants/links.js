const links = [
  {
    link: 'Dashboard',
    url: '/dashboard',
  },
  {
    link: 'Blog',
    url: '/blog',
  },
  {
    link: 'Pictures',
    url: '/pictures',
  },
  {
    link: 'Messages',
    url: '/messages',
  },
  {
    link: 'Users',
    url: '/users',
  },
  {
    link: 'Profile',
    url: '/profile',
  },
  {
    link: 'About',
    url: '/about',
  },
];

export default links;
