export const sortBlogOptionsAll = [
  { value: 'newest', label: 'Newest posts' },
  { value: 'oldest', label: 'Oldest posts' },
  { value: 'firstName', label: 'Author first name' },
  { value: 'lastName', label: 'Author last lame' },
];

export const sortBlogOptionsUser = [
  { value: 'oldest', label: 'Oldest posts' },
  { value: 'newest', label: 'Newest posts' },
];

export const sortUserOptions = [
  { value: 'newest', label: 'Newest members' },
  { value: 'oldest', label: 'Oldest members' },
  { value: 'firstName', label: 'First name' },
  { value: 'lastName', label: 'Last name' },
];

export const limitOptions = [
  { value: 5, label: 'Five' },
  { value: 10, label: 'Ten' },
  { value: 20, label: 'Twenty' },
  { value: 50, label: 'Fifty' },
];
