import { formatDateOnly } from 'Utils/functions';

const getProfileInfo = (user) => {
  return [
    {
      label: 'First name',
      field: 'first_name',
      value: user.userInfo.first_name,
    },
    {
      label: 'Last name',
      field: 'last_name',
      value: user.userInfo.last_name,
    },
    {
      label: 'Email',
      field: 'email',
      value: user.userInfo.email,
    },
    {
      label: 'City',
      field: 'city',
      value: user.userInfo.city,
    },
    {
      label: 'State',
      field: 'state',
      value: user.userInfo.state,
    },
    {
      label: 'Country',
      field: 'country',
      value: user.userInfo.country,
    },
    {
      label: 'Birthday',
      field: 'birthday',
      value: user.userInfo.birthday,
    },
    {
      label: 'Favorite color',
      field: 'favorite_color',
      value: user.userInfo.favorite_color,
    },
    {
      label: 'Hobbies',
      field: 'hobbies',
      value: user.userInfo.hobbies,
    },
    {
      label: 'Member since',
      field: 'created_at',
      value: formatDateOnly(user.userInfo.created_at),
    },
  ];
};

export default getProfileInfo;
