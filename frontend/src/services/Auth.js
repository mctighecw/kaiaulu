import { dispatch } from './Redux';
import { setAuthenticatedStatus, setAuthenticationError } from 'Actions/authActions';
import { setUserInfo, clearUserInfo } from 'Actions/mainActions';
import { getUrl } from 'Network/urls';
import { requestGet, requestPost } from 'Network/requests';

export const register = (data) => {
  return new Promise((resolve, reject) => {
    const url = getUrl('register');

    requestPost(url, data)
      .then((res) => {
        resolve(res);
        console.log('Successfully registered new user.');
      })
      .catch((err) => {
        reject(res.text);
      });
  });
};

export const login = (email, password, loginInfo) => {
  const url = getUrl('login');
  const data = JSON.stringify({
    email,
    password,
    login_info: loginInfo,
  });

  requestPost(url, data)
    .then((res) => {
      console.log(res);
      if (res.auth) {
        dispatch(setUserInfo(res.user_info));
        dispatch(setAuthenticatedStatus(true));
        window.location.reload();
        console.log('Successfully logged in.');
      }
    })
    .catch((err) => {
      console.log(err);

      if (err && err.status) {
        switch (err.status) {
          case 401:
            dispatch(setAuthenticationError('Wrong username or password'));
            break;
          case 500:
            dispatch(setAuthenticationError('Service not available'));
            break;
          default:
            dispatch(setAuthenticationError('An error has occurred'));
            console.log(err.data.status);
        }
      } else {
        dispatch(setAuthenticationError('Error connecting to server'));
      }
    });
};

export const checkAuthentication = () => {
  return new Promise((resolve, reject) => {
    const url = getUrl('check_auth');

    requestGet(url)
      .then((res) => {
        if (res.auth) {
          dispatch(setUserInfo(res.user_info));
          dispatch(setAuthenticatedStatus(true));
        }
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const logout = () => {
  const url = getUrl('logout');

  requestGet(url)
    .then((res) => {
      dispatch(setAuthenticatedStatus(false));
      dispatch(clearUserInfo());
      window.location.reload();
      console.log('Successfully logged out.');
    })
    .catch((err) => {
      console.log(err);
    });
};
