#!/usr/bin/env bash

echo Deleting old dev db and user...

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then
    db_user=postgres
else
    db_user=$USER
fi

sudo -u $db_user psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'kaiaulu_db' AND pid <> pg_backend_pid();"
sudo -u $db_user dropdb kaiaulu_db
sudo -u $db_user dropuser kaiaulu_user
