seed_users = [
    {
        'email': 'john@smith.com',
        'first_name': 'John',
        'last_name': 'Smith',
        'city': 'Honolulu',
        'state': 'HI',
        'country': 'USA',
        'birthday': '1-15-1978',
        'favorite_color': 'Blue',
        'hobbies': 'Swimming, Hiking, Reading',
        'is_admin': True
    },
    {
        'email': 'susan@williams.com',
        'first_name': 'Susan',
        'last_name': 'Williams',
        'city': 'Los Angeles',
        'state': 'CA',
        'country': 'USA',
        'birthday': '4-8-1987',
        'favorite_color': 'Green',
        'hobbies': 'Jogging, Art',
    },
    {
        'email': 'sam@jones.com',
        'first_name': 'Sam',
        'last_name': 'Jones',
        'city': 'Boston',
        'state': 'MA',
        'country': 'USA',
        'birthday': '9-21-1985',
        'favorite_color': 'Red',
        'hobbies': 'Skiing, Running, Writing',
    }
]

dev_password = 'dev'

seed_posts = [
    {
        'title': 'A philosophical thought',
        'body': 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it?',
        'user_id': 1
    },
    {
        'title': 'A story I wrote',
        'body': 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen.',
        'user_id': 2
    },
    {
        'title': 'Memories',
        'body': 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.',
        'user_id': 3
    }
]

seed_messages = [
    {
        'to_user': 1,
        'from_user': 2,
        'subject': 'Hi there',
        'body': 'How are you? Please send me a note when you have time.',
    },
    {
        'to_user': 2,
        'from_user': 3,
        'subject': 'Hey',
        'body': 'I haven\'t heard from you in a while. What\'s new?',
    },
    {
        'to_user': 3,
        'from_user': 1,
        'subject': 'Checking in',
        'body': 'Congratulations on winning the lotto! How was your vacation? Post some pictures, when you have time.',
    }
]
