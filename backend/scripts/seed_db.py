import sys
import os

sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

import src.models as models
from src.app import create_minimal_app
from src.database import db
from scripts.seeds_data import seed_users, dev_password, seed_posts, seed_messages

app = create_minimal_app()

with app.app_context():
    db.drop_all()
    db.create_all()

    for user in seed_users:
        new_user = models.User(**user)
        db.session.add(new_user)
        new_user.set_password(dev_password)
        db.session.commit()

    for post in seed_posts:
        new_post = models.Post(**post)
        db.session.add(new_post)
        db.session.commit()

    for message in seed_messages:
        new_message = models.Message(**message)
        db.session.add(new_message)
        db.session.commit()
