#!/usr/bin/env bash

echo Setting up database...

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then
    db_user=postgres
else
    db_user=$USER
fi

echo Creating user: kaiaulu_user. Please enter password "'"dev"'"
sudo -u $db_user createuser -P kaiaulu_user

echo Closing any open db connections...
sudo -u $db_user psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'kaiaulu_db' AND pid <> pg_backend_pid();"

echo Dropping old db...
sudo -u $db_user dropdb kaiaulu_db

echo Creating new db...
sudo -u $db_user createdb -O kaiaulu_user -E "utf8" -T "template0" kaiaulu_db

echo Initializing db...
FLASK_ENV="development" python scripts/seed_db.py
