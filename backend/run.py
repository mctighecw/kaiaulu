import os
from src.app import create_app

if __name__ == "__main__":
    app = create_app()

    FLASK_ENV = os.getenv("FLASK_ENV")
    port_number = app.config["APP_PORT"]
    development_mode = FLASK_ENV == "development"

    print(f"--- Starting web server on port {port_number}")

    app.run(port=port_number, host="0.0.0.0", debug=development_mode)
