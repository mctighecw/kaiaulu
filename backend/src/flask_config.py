import logging
from src.env_config import (FLASK_ENV, DB_USER, DB_PW_DEV, DB_PW_PROD,
                            DB_SERVER_DEV, DB_SERVER_PROD, DB_NAME)

# Flask
APP_PORT = 9000

# SQLAlchemy
if FLASK_ENV == 'development':
    DB_PW = DB_PW_DEV
    DB_SERVER = DB_SERVER_DEV
else:
    DB_PW = DB_PW_PROD
    DB_SERVER = DB_SERVER_PROD

SQLALCHEMY_DATABASE_URI = f'postgresql://{DB_USER}:{DB_PW}@{DB_SERVER}/{DB_NAME}'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Logging
if FLASK_ENV == 'development':
    LOG_LEVEL = logging.DEBUG
else:
    LOG_LEVEL = logging.INFO
