import bcrypt
from datetime import datetime
from src.database import db


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String, unique=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    city = db.Column(db.String, default="")
    state = db.Column(db.String, default="")
    country = db.Column(db.String, default="")
    birthday = db.Column(db.String, default="")
    favorite_color = db.Column(db.String, default="")
    hobbies = db.Column(db.String, default="")
    pic_url = db.Column(db.String, default="")
    nr_logins = db.Column(db.Integer, default=0)
    is_admin = db.Column(db.Boolean, default=False)
    password_hash = db.Column(db.String)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def set_password(self, password):
        self.password_hash = bcrypt.hashpw(password.encode("utf-8"),bcrypt.gensalt()).decode("utf-8")

    def check_password(self, password):
        return bcrypt.checkpw(password.encode("utf-8"), self.password_hash.encode("utf-8"))

    def get_full_name(self):
        return " ".join((self.first_name, self.last_name))


class Post(db.Model):
    __tablename__ = "posts"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), index=True)
    title = db.Column(db.String)
    body = db.Column(db.String)
    deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)


class Message(db.Model):
    __tablename__ = "messages"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    to_user = db.Column(db.Integer, db.ForeignKey("users.id"))
    from_user = db.Column(db.Integer, db.ForeignKey("users.id"))
    subject = db.Column(db.String)
    body = db.Column(db.String)
    unread = db.Column(db.Boolean, default=True)
    trash = db.Column(db.Boolean, default=False)
    deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)


class Picture(db.Model):
    __tablename__ = "pictures"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    pic_url = db.Column(db.String)
    caption = db.Column(db.String, default="")
    likes = db.Column(db.Integer, default=0)
    deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)


class LoginInfo(db.Model):
    __tablename__ = "login_info"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user_agent = db.Column(db.String)
    language = db.Column(db.String)
    ip = db.Column(db.String)
    isp = db.Column(db.String)
    connection_type = db.Column(db.String)
    city = db.Column(db.String)
    region = db.Column(db.String)
    country = db.Column(db.String)
    timezone = db.Column(db.String)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class WeatherInfo(db.Model):
    __tablename__ = "weather_info"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    location = db.Column(db.String)
    general = db.Column(db.String)
    description = db.Column(db.String)
    temp = db.Column(db.Float)
    max_temp = db.Column(db.Float)
    min_temp = db.Column(db.Float)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
