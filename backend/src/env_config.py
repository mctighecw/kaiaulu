import os
from pathlib import Path
from dotenv import load_dotenv

FLASK_ENV = os.getenv("FLASK_ENV")

if FLASK_ENV == "development":
    root_dir = Path(__file__).parent.parent.parent
    env_file = os.path.join(root_dir, ".env")
    env_db_file = os.path.join(root_dir, ".env_db")
    load_dotenv(env_file)
    load_dotenv(env_db_file)

JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
OWM_KEY = os.getenv("OWM_KEY")
CLOUD_NAME = os.getenv("CLOUD_NAME")
CLOUDINARY_KEY = os.getenv("CLOUDINARY_KEY")
CLOUDINARY_SECRET = os.getenv("CLOUDINARY_SECRET")

DB_USER = os.getenv("DB_USER")
DB_NAME = os.getenv("DB_NAME")
DB_PW_DEV = os.getenv("DB_PW_DEV")
DB_PW_PROD = os.getenv("DB_PW_PROD")
DB_SERVER_DEV = os.getenv("DB_SERVER_DEV")
DB_SERVER_PROD = os.getenv("DB_SERVER_PROD")
