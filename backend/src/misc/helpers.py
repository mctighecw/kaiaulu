def make_list(object):
    """
    Converts an SQLAlchemy object to a list.
    """
    result = []
    for i in object:
        d = i.__dict__
        d.pop("_sa_instance_state", None)
        result.append(d)
    return result


def get_file_extension(filename):
    """
    Gets the file extension.
    """
    return "." in filename and filename.rsplit(".", 1)[1].lower()


def allowed_file_upload(filename):
    """
    Allowed image types for picture upload.
    """
    ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "gif"])
    extension = get_file_extension(filename)
    return extension in ALLOWED_EXTENSIONS


def check_valid_location(location):
    """
    Checks if a location is valid or not.
    """
    return len(location) > 2
