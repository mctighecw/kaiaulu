import pyowm
from datetime import datetime
from flask import jsonify

import src.models as models
from src.database import db

from src.misc.constants import default_headers, status_error
from src.env_config import OWM_KEY


def get_user_weather(user_id, location):
    """
    Open Weather Map API weather data request.
    """
    print("Making call to OWM API")

    owm = pyowm.OWM(OWM_KEY)

    try:
        observation = owm.weather_at_place(location)
        w = observation.get_weather()

        general = w.get_status()
        description = w.get_detailed_status()
        temp = w.get_temperature("celsius")

        weather_data = {
            "location": location,
            "general": general,
            "description": description,
            "temp": {
                "temp": temp["temp"],
                "max_temp": temp["temp_max"],
                "min_temp": temp["temp_min"]
            }
        }

        save_user_weather(user_id, weather_data)

        res = jsonify({"status": "OK", "weather": weather_data})
        return res, 200, default_headers
    except:
        res = jsonify({"status": "Error", "message": "Invalid location submitted"})
        return res, 400, default_headers


def save_user_weather(user_id, weather_data):
    """
    Saves weather data for user in database.
    """
    user_weather = db.session.query(models.WeatherInfo). \
        filter(models.WeatherInfo.user_id == user_id). \
        one_or_none()

    if user_weather is not None:
         # Update existing weather info for user's row
        user_weather.location = weather_data["location"]
        user_weather.general = weather_data["general"]
        user_weather.description = weather_data["description"]
        user_weather.temp = weather_data["temp"]["temp"]
        user_weather.max_temp = weather_data["temp"]["max_temp"]
        user_weather.min_temp = weather_data["temp"]["min_temp"]
        user_weather.updated_at = datetime.utcnow()

        db.session.commit()
        print("Weather data sucessfully updated")

    else:
        # New database row for user weather info
        new_weather = models.WeatherInfo(user_id=user_id, location=weather_data["location"], general=weather_data["general"],
            description=weather_data["description"], temp=weather_data["temp"]["temp"], max_temp=weather_data["temp"]["max_temp"],
            min_temp=weather_data["temp"]["min_temp"])

        db.session.add(new_weather)
        db.session.commit()
        print("New weather data sucessfully saved")
