import bleach
from html import unescape
from flask import request


def get_json():
    data = request.get_json(force=True, silent=False)
    d = clean_dict(data)
    return d


def clean_dict(d):
    for k, v in d.items():
        d[k] = sanitize(v)
    return d


def sanitize(v):
    """
    Checks and cleans user input for potentially harmful code.
    """
    if type(v) is int:
        return v
    if type(v) is float:
        return v
    if type(v) is bool:
        return v
    if type(v) is dict:
        v = clean_dict(v)
        return v
    return unescape(bleach.clean(v, strip=True))
