import src.models as models

def get_user_info(email):
    """
    Returns a dictionary with complete user information.
    """
    user = models.User.query.filter(models.User.email == email).first()
    user_info = {
        "id": user.id,
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "city": user.city,
        "state": user.state,
        "country": user.country,
        "birthday": user.birthday,
        "favorite_color": user.favorite_color,
        "hobbies": user.hobbies,
        "pic_url": user.pic_url,
        "is_admin": user.is_admin,
        "created_at": user.created_at
    }
    return user_info
