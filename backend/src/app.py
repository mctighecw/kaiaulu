import os
import logging

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from src import flask_config
from src.database import db
from src.routes.auth import auth
from src.routes.user import user
from src.routes.post import post
from src.routes.message import message
from src.routes.picture import picture
from src.env_config import JWT_SECRET_KEY


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)
    FLASK_ENV = os.getenv("FLASK_ENV")

    # allow any origin for development environment
    if FLASK_ENV == "development":
        CORS(app)

    # configure logger
    logging.basicConfig(format="%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s",
                        level=app.config["LOG_LEVEL"])

    # init db connection
    db.init_app(app)

    # API base route
    app.config["APPLICATION_ROOT"] = "/api/"

    # JWT config
    # configure application to store JWTs in cookies
    app.config["JWT_TOKEN_LOCATION"] = ["cookies"]

    # only allow JWT cookies to be sent over https
    if FLASK_ENV == "development":
        secure_cookies = False
    else:
        secure_cookies = True

    app.config["JWT_COOKIE_SECURE"] = secure_cookies

    # set the cookie paths
    app.config["JWT_ACCESS_COOKIE_PATH"] = "/api/"
    app.config["JWT_REFRESH_COOKIE_PATH"] = "/api/auth/refresh"

    # disable CSRF protection
    app.config["JWT_COOKIE_CSRF_PROTECT"] = False

    # set the secret key to sign JWTs
    app.config["JWT_SECRET_KEY"] = JWT_SECRET_KEY

    jwt = JWTManager(app)

    app.register_blueprint(auth, url_prefix="/api/auth")
    app.register_blueprint(user, url_prefix="/api/user")
    app.register_blueprint(post, url_prefix="/api/post")
    app.register_blueprint(message, url_prefix="/api/message")
    app.register_blueprint(picture, url_prefix="/api/picture")

    return app


def create_minimal_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)
    logging.basicConfig(format="%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s",
                        level=app.config["LOG_LEVEL"])
    db.init_app(app)
    return app
