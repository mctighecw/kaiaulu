from datetime import datetime
from sqlalchemy import and_
from sqlalchemy.orm import aliased
from flask import request, Blueprint, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

import src.models as models
from src.database import db
from src.misc.sanitize import get_json
from src.misc.constants import default_headers, status_ok

message = Blueprint("message", __name__)


@message.before_request
def before_request():
    pass


@message.route("/get_all_user_list", methods=["GET"])
@jwt_required
def get_all_user_list():
    """
    Gets user list, except selected user, and returns full name and id.
    """
    email = get_jwt_identity()
    users = models.User.query.filter(models.User.email != email).order_by(models.User.first_name.asc()).all()

    all_users = []
    for u in users:
        d = u.__dict__
        id = d["id"]
        name = u.get_full_name()
        all_users.append({ "id": id, "name": name })

    res = jsonify({"status": "OK", "all_users": all_users })
    return res, 200, default_headers


@message.route("/new_message", methods=["POST"])
@jwt_required
def new_message():
    """
    Creates new user message.
    """
    data = get_json()

    email = get_jwt_identity()
    user = models.User.query.filter(models.User.email == email).one_or_none()
    message = models.Message(to_user=data["to_user"], from_user=user.id, subject=data["subject"], body=data["body"])

    db.session.add(message)
    db.session.commit()

    res = jsonify({"status": "New message created"})
    return res, 200, default_headers


@message.route("/get_messages/<string:type>", methods=["GET"])
@jwt_required
def get_messages(type):
    """
    Gets user messages by type (received, sent, deleted).
    """
    email = get_jwt_identity()
    user = models.User.query.filter(models.User.email == email).one_or_none()

    if type == "sent":
        criterion1 = models.Message.from_user
        criterion2 = False
    elif type == "received":
        criterion1 = models.Message.to_user
        criterion2 = False
    elif type == "deleted":
        criterion1 = models.Message.to_user
        criterion2 = True


    message_list = []
    user_alias = aliased(models.User)

    # get messages with full names of to and from users
    for message, user1, user2 in db.session. \
        query(models.Message, models.User, user_alias). \
        filter(and_(criterion1 == user.id,
                    models.Message.trash == criterion2,
                    models.Message.deleted.is_(False),
                    models.Message.to_user == models.User.id,
                    models.Message.from_user == user_alias.id)). \
        order_by(models.Message.created_at.desc()). \
        all():

        msg = {
            "id": message.id,
            "subject": message.subject,
            "body": message.body,
            "to": user1.get_full_name(),
            "from": user2.get_full_name(),
            "unread": message.unread,
            "created_at": message.created_at
        }

        message_list.append(msg)

    res = jsonify({"status": "OK", "messages": message_list})
    return res, 200, default_headers


@message.route("/delete_message", methods=["POST"])
@jwt_required
def delete_message():
    """
    Marks user message as in trash and then deletes it (but not from database).
    """
    data = request.get_json(force=True, silent=False)
    type = data["type"]
    message_id = data["message_id"]

    email = get_jwt_identity()
    user = models.User.query.filter(models.User.email == email).one_or_none()
    message = models.Message.query.filter(models.Message.id == message_id).one_or_none()

    if message.to_user == user.id:
        if type == "delete":
            # either mark message as trash or mark as deleted
            if message.trash == True:
                message.deleted = True
            else:
                message.trash = True

            message.deleted_at = datetime.utcnow()
            db.session.commit()

            res = jsonify({"status": "OK", "message": "Message deleted"})
            return res, 200, default_headers
        elif type == "undelete":
            # unmark message as trash
            message.trash = False
            message.deleted_at = None
            db.session.commit()

            res = jsonify({"status": "OK", "message": "Message undeleted"})
            return res, 200, default_headers
    else:
        res = jsonify({"status": "unauthorized"})
        return res, 401, default_headers


@message.route("/get_unread_number", methods=["GET"])
@jwt_required
def get_unread_number():
    """
    Gets number of unread messages for a user.
    """
    email = get_jwt_identity()
    user = models.User.query.filter(models.User.email == email).one_or_none()

    unread_count = models.Message.query.filter(and_(models.Message.to_user == user.id, \
                                                    models.Message.unread.is_(True))). \
                                        count()

    res = jsonify({"status": "OK", "unread_count": unread_count})
    return res, 200, default_headers


@message.route("/toggle_unread", methods=["POST"])
@jwt_required
def toggle_unread():
    """
    Toggle a message from unread to read (or vice versa).
    """
    data = request.get_json(force=True, silent=False)
    type = data["type"]
    message_id = data["message_id"]
    email = get_jwt_identity()

    user = models.User.query.filter(models.User.email == email).one_or_none()

    if type == "single":
        message = models.Message.query.filter(models.Message.id == message_id).one_or_none()

        if message.to_user == user.id:
            value = message.unread
            message.unread = not value
            db.session.commit()

            return jsonify(status_ok), 200, default_headers
        else:
            res = jsonify({"status": "not allowed"})
            return res, 401, default_headers

    elif type == "all":
        all_messages = models.Message.query.filter(models.Message.to_user == user.id).all()

        for message in all_messages:
            message.unread = False
            db.session.commit()

        return jsonify(status_ok), 200, default_headers
