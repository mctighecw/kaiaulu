from flask import Blueprint, jsonify
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, \
    jwt_refresh_token_required, get_jwt_identity, set_access_cookies, \
    set_refresh_cookies, unset_jwt_cookies)

import src.models as models
from src.database import db
from src.misc.user_functions import get_user_info
from src.misc.sanitize import get_json
from src.misc.constants import default_headers

auth = Blueprint("auth", __name__)


@auth.before_request
def before_request():
    pass


@auth.route("/register", methods=["POST"])
def register():
    """
    Registers a new user.
    """
    data = get_json()

    if all(k in data for k in ["first_name", "last_name", "email", "password"]):
        email = data["email"].lower()

        exist_email = models.User.query.filter(models.User.email == email).first()

        if not exist_email:
            user = models.User(first_name=data["first_name"], last_name=data["last_name"],
                               email=email)
            user.set_password(data["password"])

            db.session.add(user)
            db.session.commit()

            res = jsonify({"status": "User has successfully registered"})
            return res, 200, default_headers
        else:
            res = jsonify({"status": "Email address is already taken"})
            return res, 409, default_headers
    else:
        res = jsonify({"status": "Please provide all required fields"})
        return res, 400, default_headers


@auth.route("/login", methods=["POST"])
def login():
    """
    Logs the user in.
    """
    data = get_json()
    email = data["email"]
    password = data["password"]
    login_info = data["login_info"]

    current_user = models.User.query.filter(models.User.email == email).first()

    if current_user is not None and current_user.check_password(password):
        login_info["user_id"] = current_user.id
        login_list = []
        login_list.append(login_info)

        # record user login info
        for item in login_list:
            new_login_info = models.LoginInfo(**item)
            db.session.add(new_login_info)

        current_user.nr_logins += 1
        db.session.commit()

        # jwt authentication and user info
        access_token = create_access_token(identity=email)
        refresh_token = create_refresh_token(identity=email)
        user_info = get_user_info(email)
        res = jsonify({"status": "User logged in", "user_info": user_info, "auth": True})
        set_access_cookies(res, access_token)
        set_refresh_cookies(res, refresh_token)
        return res, 200, default_headers
    else:
        res = jsonify({"status": "Wrong login credentials", "auth": False})
        return res, 401, default_headers


@auth.route("/check", methods=["GET"])
@jwt_required
def check():
    """
    Checks validity of user JWT and returns user info.
    """
    try:
        email = get_jwt_identity()
        access_token = create_access_token(identity=email)
        current_user = models.User.query.filter(models.User.email == email).first()
        user_info = get_user_info(email)
        res = jsonify({"status": "User is still logged in", "user_info": user_info, "auth": True})
        set_access_cookies(res, access_token)
        return res, 200, default_headers
    except:
        return 401, default_headers


@auth.route("/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    """
    Refreshes a valid JWT.
    """
    email = get_jwt_identity()
    access_token = create_access_token(identity=email)
    res = jsonify({"status": "JWT refreshed", "auth": True})
    set_access_cookies(res, access_token)
    return res, 200, default_headers


@auth.route("/change_password", methods=["POST"])
@jwt_required
def change_password():
    """
    Changes the user's password (password_hash in database)
    """
    data = get_json()
    old_password = data["old_password"]
    new_password = data["new_password"]

    email = get_jwt_identity()
    current_user = models.User.query.filter(models.User.email == email).first()

    pw_valid = current_user.check_password(old_password)

    if pw_valid:
        current_user.set_password(new_password)
        db.session.commit()
        res = jsonify({"status": "OK", "message": "User has successfully changed password"})
        return res, 200, default_headers
    else:
        res = jsonify({"status": "Error", "message": "Wrong password submitted"})
        return res, 401, default_headers


@auth.route("/logout", methods=["GET"])
def logout():
    """
    Logs the user out.
    """
    res = jsonify({"status": "User logged out", "auth": False})
    unset_jwt_cookies(res)
    return res, 200, default_headers
