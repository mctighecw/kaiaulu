import os
from datetime import datetime
from sqlalchemy import and_
from flask import request, Blueprint, jsonify
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required, get_jwt_identity

import cloudinary
import cloudinary.uploader
from src.env_config import FLASK_ENV, CLOUD_NAME, CLOUDINARY_KEY, CLOUDINARY_SECRET

import src.models as models
from src.database import db
from src.misc.helpers import get_file_extension, allowed_file_upload
from src.misc.sanitize import sanitize
from src.misc.constants import default_headers, status_ok

picture = Blueprint("picture", __name__)

cloudinary.config(
    cloud_name=CLOUD_NAME,
    api_key=CLOUDINARY_KEY,
    api_secret=CLOUDINARY_SECRET
)


@picture.before_request
def before_request():
    pass


@picture.route("/upload_picture", methods=["POST"])
@jwt_required
def upload_picture():
    """
    Uploads an image file to Cloudinary (cloud image service) using personal key.
    """
    email = get_jwt_identity()
    file = request.files["file"]
    type = request.form["type"]

    if file and allowed_file_upload(file.filename):
        try:
            filename = secure_filename(file.filename)
            extension = get_file_extension(file.filename)

            user = db.session.query(models.User).filter(models.User.email == email).one_or_none()
            nr = db.session.query(models.Picture).count() + 1
            pic_nr = "{:06d}".format(nr)

            # profile / general variables
            if type == "profile":
                folder = "kaiaulu/profile/"
                new_filename = "_".join((str(user.id), user.first_name, user.last_name))
            else:
                folder = "kaiaulu/general/"
                new_filename = str(pic_nr)

            # need to save file first
            root_dir = os.path.abspath(os.curdir)

            if FLASK_ENV == "development":
                saved_file = os.path.join(root_dir, "backend", "uploads", file.filename)
            else:
                saved_file = os.path.join(root_dir, "uploads", file.filename)

            file.save(saved_file)

            # save info to db
            if type == "profile":
                user.pic_url = "".join((folder, new_filename, ".", extension))
            else:
                caption = sanitize(request.form["caption"])
                pic_url = "".join((folder, new_filename, ".", extension))
                new_picture = models.Picture(user_id=user.id, pic_url=pic_url, caption=caption)
                db.session.add(new_picture)

            db.session.commit()

            # upload file to cloudinary
            cloudinary.uploader.upload(saved_file,
                folder = folder,
                public_id = new_filename,
                overwrite = True)

            # remove saved file
            os.remove(saved_file)

            return jsonify(status_ok), 200, default_headers
        except:
            res = jsonify({"status": "Error with upload"})
            return res, 400, default_headers
    else:
        res = jsonify({"status": "Error with file"})
        return res, 400, default_headers


@picture.route("/get_pictures/<string:type>", methods=["GET"])
@jwt_required
def get_pictures(type):
    """
    Gets images urls and info from database.
    """
    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()
    pictures_list = []

    if type == "user":
        for picture, pic_user in db.session.query(models.Picture, models.User). \
            filter(and_(models.Picture.deleted.is_(False),
                        models.Picture.user_id == user.id,
                        models.Picture.user_id == models.User.id)). \
            all():

            pic = {
                "id": picture.id,
                "user_id": pic_user.id,
                "user_name": pic_user.get_full_name(),
                "pic_url": picture.pic_url,
                "caption": picture.caption,
                "likes": picture.likes,
                "created_at": picture.created_at
            }
            pictures_list.append(pic)
    else:
        for picture, pic_user in db.session.query(models.Picture, models.User). \
            filter(and_(models.Picture.deleted.is_(False),
                        models.Picture.user_id == models.User.id)). \
            all():

            pic = {
                "id": picture.id,
                "user_name": pic_user.get_full_name(),
                "pic_url": picture.pic_url,
                "caption": picture.caption,
                "likes": picture.likes,
                "created_at": picture.created_at
            }
            pictures_list.append(pic)

    res = jsonify({"status": "OK", "pictures": pictures_list})
    return res, 200, default_headers


@picture.route("/like_picture", methods=["POST"])
@jwt_required
def like_picture():
    """
    Add a user "like" to a picture.
    """
    data = request.get_json(force=True, silent=False)
    picture_id = data["id"]

    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()

    picture = models.Picture.query.filter(models.Picture.id == picture_id).one_or_none()
    picture.likes += 1
    db.session.commit()

    return jsonify(status_ok), 200, default_headers


@picture.route("/delete_picture", methods=["POST"])
@jwt_required
def delete_picture():
    """
    Marks picture as deleted in database.
    """
    data = request.get_json(force=True, silent=False)
    picture_id = data["id"]

    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()
    picture = models.Picture.query.filter(models.Picture.id == picture_id).one_or_none()

    if (user.id == picture.user_id) or user.is_admin:
        picture.deleted = True
        picture.deleted_at = datetime.utcnow()
        db.session.commit()

        return jsonify(status_ok), 200, default_headers
    else:
        res = jsonify({"status": "Access denied"})
        return res, 400, default_headers


@picture.route("/new_picture_count", methods=["GET"])
@jwt_required
def new_picture_count():
    """
    Gets number of pictures uploaded since last user login.
    """
    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()

    last_session = db.session.query(models.LoginInfo). \
        filter(models.LoginInfo.user_id == user.id). \
        order_by(models.LoginInfo.created_at.desc()). \
        limit(2)

    if last_session.count() > 1:
        # check if the user has logged in before
        last_login_datetime = last_session[1].created_at
        new_pic_count = db.session.query(models.Picture). \
            filter(and_(models.Picture.deleted.is_(False),
                        models.Picture.created_at > last_login_datetime)). \
            count()
    else:
        new_pic_count = 0

    res = jsonify({"status": "OK", "new_pic_count": new_pic_count })
    return res, 200, default_headers
