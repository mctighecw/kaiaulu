from datetime import datetime, timedelta
from flask import request, Blueprint, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

import src.models as models
from src.database import db

from src.misc.helpers import check_valid_location
from src.misc.user_functions import get_user_info
from src.misc.weather_functions import get_user_weather
from src.misc.sanitize import get_json
from src.misc.constants import default_headers

user = Blueprint("user", __name__)


@user.before_request
def before_request():
    pass


@user.route("/get_counts", methods=["GET"])
@jwt_required
def get_counts():
    """
    Retrieves the counts of users, posts, messages, and pictures in database.
    """
    users = db.session.query(models.User).count()
    posts = db.session.query(models.Post).filter(models.Post.deleted.is_(False)).count()
    messages = db.session.query(models.Message).count()
    pictures = db.session.query(models.Picture).filter(models.Picture.deleted.is_(False)).count()

    res = jsonify({"counts": {"users": users, "posts": posts, "messages": messages,
            "pictures": pictures}, "status": "OK"})
    return res, 200, default_headers


@user.route("/update_profile", methods=["POST"])
@jwt_required
def update_profile():
    """
    Updates user profile info.
    """
    data = get_json()

    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()

    if user:
        for k in data:
            setattr(user, k, data[k])

        db.session.add(user)
        db.session.commit()

        user_info = get_user_info(email)
        res = jsonify({"status": "User info successfully updated", "user_info": user_info})
        return res, 200, default_headers
    else:
        res = jsonify({"status": "User not found"})
        return res, 400, default_headers


@user.route("/get_users", methods=["POST"])
@jwt_required
def get_users():
    """
    Gets users profiles according to sort by criterion and max number.
    """
    data = request.get_json(force=True, silent=False)
    sort_by = data["sort_by"]
    max_number = data["max_number"]
    email = get_jwt_identity()

    if sort_by == "newest":
        criterion = models.User.created_at.desc()
    elif sort_by == "oldest":
        criterion = models.User.created_at.asc()
    elif sort_by == "firstName":
        criterion = models.User.first_name.asc()
    elif sort_by == "lastName":
        criterion = models.User.last_name.asc()

    users = db.session.query(models.User). \
                            filter(models.User.email != email). \
                            order_by(criterion). \
                            limit(max_number)
    users_list = []

    for user in users:
        new_user = {
            "id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "city": user.city,
            "state": user.state,
            "country": user.country,
            "birthday": user.birthday,
            "favorite_color": user.favorite_color,
            "hobbies": user.hobbies,
            "pic_url": user.pic_url,
            "created_at": user.created_at
        }
        users_list.append(new_user)

    res = jsonify({"status": "OK", "users": users_list})
    return res, 200, default_headers


@user.route("/get_weather_info", methods=["GET"])
@jwt_required
def get_weather_info():
    """
    Gets weather info for user either from database, or based on profile / login location.
    Weather info fetched and saved in database when more than 5 minutes old.
    """
    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()

    user_weather = db.session.query(models.WeatherInfo). \
        filter(models.WeatherInfo.user_id == user.id). \
        one_or_none()

    if user_weather is not None:
        # Check if saved weather data is more than 5 minutes old
        old_data = user_weather.updated_at < (datetime.utcnow() - timedelta(minutes=5))

        if not old_data:
            print("Returning weather saved in database that is less than 5 min old")
            weather_data = {
                "location": user_weather.location,
                "general": user_weather.general,
                "description": user_weather.description,
                "temp": {
                    "temp": user_weather.temp,
                    "max_temp": user_weather.max_temp,
                    "min_temp": user_weather.min_temp
                }
            }

            res = jsonify({"status": "OK", "weather": weather_data})
            return res, 200, default_headers
        else:
            print("Weather data is more than 5 min old and is being updated")
            location = user_weather.location
            weather_res = get_user_weather(user.id, location)
            return weather_res

    else:
        # Fetch new data based on profile or login location
        # Find last user login location
        user_location = db.session.query(models.LoginInfo). \
            filter(models.LoginInfo.user_id == user.id). \
            order_by(models.LoginInfo.created_at.desc()). \
            limit(1). \
            one_or_none()

        if user is not None and check_valid_location(user.city) and check_valid_location(user.country):
            # Use user profile location
            city = user.city
            country = user.country
            location = ", ".join((city, country))
        elif user_location is not None and check_valid_location(user_location.city) and \
                                            check_valid_location(user_location.country):
            # Use user login location
            city = user_location.city
            country = user_location.country
            location = ", ".join((city, country))
        else:
            location = ""
            print("No user profile or login location data available")

        if check_valid_location(location):
            weather_res = get_user_weather(user.id, location)
            return weather_res
        else:
            res = jsonify({"status": "No location available"})
            return res, 406, default_headers


@user.route("/request_weather_info", methods=["POST"])
@jwt_required
def request_weather_info():
    """
    Gets weather info based on user request.
    """
    data = get_json()
    city = data["city"]
    country = data["country"]
    location = ", ".join((city, country))

    email = get_jwt_identity()
    user = db.session.query(models.User).filter(models.User.email == email).one_or_none()

    user_weather = db.session.query(models.WeatherInfo). \
        filter(models.WeatherInfo.user_id == user.id). \
        one_or_none()

    if user_weather is None:
        weather_res = get_user_weather(user.id, location)
        return weather_res
    else:
        old_data = user_weather.updated_at < (datetime.utcnow() - timedelta(minutes=5))
        same_location = user_weather.location.lower() == location.lower()

        if same_location and old_data:
            print("Weather data is more than 5 min old and is being updated")
            weather_res = get_user_weather(user.id, location)
            return weather_res
        elif not same_location:
            print("Fetching weather data for new user location")
            weather_res = get_user_weather(user.id, location)
            return weather_res
        else:
            print("Returning existing weather data from database")
            weather_data = {
                "location": user_weather.location,
                "general": user_weather.general,
                "description": user_weather.description,
                "temp": {
                    "temp": user_weather.temp,
                    "max_temp": user_weather.max_temp,
                    "min_temp": user_weather.min_temp
                }
            }

            res = jsonify({"status": "OK", "weather": weather_data})
            return res, 200, default_headers
