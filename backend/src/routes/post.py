from datetime import datetime
from sqlalchemy import and_, or_
from flask import request, Blueprint, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

import src.models as models
from src.database import db
from src.misc.sanitize import get_json
from src.misc.constants import default_headers, status_error

post = Blueprint("post", __name__)


@post.before_request
def before_request():
    pass


@post.route("/get_blog_posts", methods=["POST"])
@jwt_required
def get_blog_posts():
    """
    Gets blog posts according to filter & search options (number_posts, sort_by, search_term).
    Based on 'type', either posts only for user or for all users.
    """
    data = get_json()
    type = data["type"]
    number_posts = data["number_posts"]
    sort_by = data["sort_by"]
    search_term = data["search_term"]

    email = get_jwt_identity()

    res = {"posts": [], "status": ""}

    # Sorting criteria
    if sort_by == "oldest":
        sort_criterion = models.Post.created_at.asc()
    elif sort_by == "newest":
        sort_criterion = models.Post.created_at.desc()
    elif sort_by == "firstName":
        sort_criterion = models.User.first_name.asc()
    elif sort_by == "lastName":
        sort_criterion = models.User.last_name.asc()

    if type == "user":
        # Get user's blog posts that match sorting & filtering criteria
        for post in db.session.query(models.Post). \
                join(models.User). \
                filter(and_(models.Post.deleted.is_(False), \
                            models.User.email == email)). \
                order_by(sort_criterion). \
                limit(number_posts):

            user = models.User.query.filter(models.User.email == email).first()
            res["posts"].append({"id": post.id,
                                "user": {
                                    "id": post.user_id,
                                    "full_name": user.get_full_name(),
                                },
                                "title": post.title,
                                "body": post.body,
                                "created_at": post.created_at.isoformat()
                                })
    else:
        # Get blog posts for all users that match sorting & filtering criteria
        filtered_posts = db.session.query(models.Post). \
            join(models.User). \
            filter(models.Post.deleted.is_(False))

        if search_term != "":
            search_str = "{}{}{}".format("%", search_term, "%")
            filtered_posts = filtered_posts. \
                            filter(or_(models.Post.title.ilike(search_str),
                                    models.Post.body.ilike(search_str)))

        filtered_posts = filtered_posts.order_by(sort_criterion).limit(number_posts)

        for post in filtered_posts:
            # Get user for each post
            user = models.User.query.filter(models.User.id == post.user_id).first()

            res["posts"].append({"id": post.id,
                                "user": {
                                    "id": post.user_id,
                                    "full_name": user.get_full_name(),
                                },
                                 "title": post.title,
                                 "body": post.body,
                                 "created_at": post.created_at.isoformat()
                                })

    res["status"] = "OK"
    res = jsonify(res)
    return res, 200, default_headers


@post.route("/get_recent_posts", methods=["GET"])
@jwt_required
def get_recent_posts():
    """
    Loads up to five most recent posts.
    """
    res = {"posts": [], "status": ""}
    max_num_posts = 5

    for post in db.session.query(models.Post). \
        filter(models.Post.deleted.is_(False)). \
        order_by(models.Post.created_at.desc()). \
        limit(max_num_posts).all():

        user = models.User.query.filter(models.User.id == post.user_id).first()
        res["posts"].append({"id": post.id,
                            "user": {
                                "id": post.user_id,
                                "full_name": user.get_full_name(),
                            },
                            "title": post.title,
                            "created_at": post.created_at.isoformat()
                            })

    res["status"] = "OK"
    res = jsonify(res)
    return res, 200, default_headers


@post.route("/create_post", methods=["POST"])
@jwt_required
def create_post():
    """
    Saves user created posts in database.
    """
    data = get_json()
    email = get_jwt_identity()

    if all(k in data for k in ["title", "body"]):
        user = db.session.query(models.User). \
            filter(models.User.email == email).first()
        post = models.Post(user_id=user.id, title=data["title"], body=data["body"])

        db.session.add(post)
        db.session.commit()

        res = jsonify({"status": "Post saved successfully"})
        return res, 200, default_headers
    else:
        return jsonify(status_error), 400, default_headers


@post.route("/delete_post", methods=["POST"])
@jwt_required
def delete_post():
    """
    Deletes a post from the database.
    """
    data = request.get_json(force=True, silent=False)
    post_id = data["id"]

    try:
        post = db.session.query(models.Post).filter(models.Post.id == post_id).one_or_none()
        post.deleted = True
        post.deleted_at = datetime.utcnow()
        db.session.commit()

        res = jsonify({"status": "Post deleted successfully"})
        return res, 200, default_headers
    except:
        return jsonify(status_error), 400, default_headers
