echo Removing all Docker containers, images, and volumes...

docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
docker rmi $(docker images -q)
docker volume rm $(docker volume ls -qf dangling=true)

docker ps -aq
docker images ls --all
docker volume ls
